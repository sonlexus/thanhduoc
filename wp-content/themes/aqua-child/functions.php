<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
         
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'bootstrap.min','flexslider.css','jquery.mCustomScrollbar','jquery.fancybox','colorbox','font-ionicons','font-aqua','uikit.min','tb.core.min','woocommerce','shortcodes','main-style','style','style','wp_custom_style' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css' );


function example_enqueue_styles() {

    // enqueue parent styles
    wp_enqueue_style('parent-theme', get_template_directory_uri() .'/style.css');

    // enqueue child styles
    wp_enqueue_style('child-theme', get_stylesheet_directory_uri() .'/style-custom.css', array('parent-theme'));

}
add_action('wp_enqueue_scripts', 'example_enqueue_styles',999);

// END ENQUEUE PARENT ACTION
