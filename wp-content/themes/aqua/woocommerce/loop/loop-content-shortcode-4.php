<article <?php post_class( 'tb-product-item' ); ?>>
				
	<div class="tb-product-item-inner"> 
		
		<?php if($show_sale_flash) do_action( 'woocommerce_template_product_loop_sale_flash' ); ?>
		
		<div class="tb-item-content-image">
		
			<?php do_action( 'woocommerce_template_loop_product_thumbnail' ); ?>
			
			<div class="tb-item-button-content">
				<div class="tb-item-button tb-action">
					<?php 	
						if( $show_add_to_cart ):
						?>
						<div class="tb-btn tb-product-btn tb-btn-tocart">
							<?php do_action( 'woocommerce_template_loop_add_to_cart' ); ?>
						</div>
						<?php endif;
						if( $show_whishlist ):
							if( function_exists('YITH_WCWL') ):
								$wishlist_text = YITH_WCWL()->is_product_in_wishlist( get_the_ID() ) ? __(' Browse Wishlist','aqua') : __('Add To Wishlist', 'aqua');
								 ?>
								<div class="tb-btn tb-product-btn tb-btn-wishlist">
									<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]');?>
								</div>
								<?php
							endif;
						endif;
					?>
				</div>
			</div>
		</div>
		<div class="tb-item-content-info">
			<div class="tb-item-content-info-inner">
				<?php if($show_title){ ?>
					<a href="<?php the_permalink(); ?>"><?php the_title( '<h4 class="text-ellipsis underline">', '</h4>' ); ?></a>
				<?php } ?>
				
				<?php if($show_price) do_action( 'woocommerce_template_product_loop_price' ); ?>
				
				<?php if($show_rating){ ?>
					<div class="tb-item-rating">
						<?php do_action( 'woocommerce_template_loop_rating' ); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

</article>