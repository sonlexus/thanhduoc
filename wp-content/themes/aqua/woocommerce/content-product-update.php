<?php
/**
* The template for displaying product content within loops.
*
* Override this template by copying it to yourtheme/woocommerce/content-product.php
*
* @author 		WooThemes
* @package 	WooCommerce/Templates
* @version     2.6.1
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $product, $woocommerce_loop, $tb_options;
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
$woocommerce_loop['loop'] = 0;
//Set columns
$columns = 4;
if (is_active_sidebar('tbtheme-shop-update-sidebar')) {
$columns = 3;
}
// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', $columns );
$class_columns = null;
switch ($woocommerce_loop['columns']) {
case 1: $class_columns = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
	break;
case 2: $class_columns = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
	break;
case 3: $class_columns = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
	break;
case 4: $class_columns = 'col-xs-12 col-sm-3 col-md-3 col-lg-3';
	break;
default: $class_columns = 'col-xs-12 col-sm-3 col-md-3 col-lg-3';
	break;
}
$class_columns .= ' tb-product-item';
// Ensure visibility
if ( ! $product || ! $product->is_visible() )
return;
// Increase loop count
$woocommerce_loop['loop']++;
$start_row = $end_row = '';
// Extra post classes
$classes = array();

$classes[] = 'row tb-product-items';
/* if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ){
$classes[] = 'first';
$start_row = 1;
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ){
$classes[] = 'last';
$end_row = 1;
} */
?>

<div class="<?php echo tb_filtercontent($class_columns); ?>">
	<?php include(locate_template('woocommerce/loop/loop-content-shortcode.php')); ?>
</div>
