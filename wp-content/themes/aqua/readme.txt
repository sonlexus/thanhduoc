=== JWSThemes ===
Author: jwsthemes
Requires at least: WordPress 4.6.0
Tested up to: WordPress 4.9.6
Version: 3.1.6
Description: 
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, right-sidebar, custom-background, custom-colors, editor-style, featured-images, microformats, post-formats, sticky-post, threaded-comments, translation-ready
== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's aqua.zip file. Click Install Now.
3. Click Activate to use your new theme right away.

CHANGELOG:

V3.1.6 – May 28, 2018
# Compatible with WordPress 5.0.x.
# Compatible with WooCommerce 3.4.x.
# Fixed small CSS.

V3.1.5 – March 05, 2018
# Compatible with WordPress 5.0.x.
# Compatible with WooCommerce 3.3.x.
# Fixed small CSS.

V3.1.5 – Sep 15, 2017
# Compatible with WC MarketPlace plugin.
# Compatible with WC Vendor plugin.
# Compatible with WC FrontEnd Manager plugin.
# Fixed small CSS.

V3.1.2 – Aug 22, 2017
# Fixed bug can't edit shortcode.
# Fixed error menu.
V3.1.0 – Aug 02, 2017
# Fixed bug menu touch mobile.
# Fixed error title.

V3.0.5 – Jul 18, 2017
# Updated One Click Demo Import
# Compatible with WooCommerce 3.1.x
# Updated Redux Framework 3.6.5
# Fixed small CSS.

V3.0.1 – April 24, 2017
# Compatible with PHP 7.0.x
V3.0.0 – April 14, 2017
# Compatible with WooCommerce 3.0.x
# Updated Redux Framework 3.6.4
# Fixed small CSS.

V2.0.0 – Jan 23, 2017
# Updated 06 Homepage: 02 Yoga Homepage, 02 Fitness Homepage, 02 Spa Homepage.
# Updated Redux Famework.
# Updated file language.
# Fixed small CSS.
# Updated sample.xml file.
# Notice: This version has been updated 6 new homepages and made few changes of CSS. If you have installed and customed CSS in your theme so far, please, skip this version. This version for the first installation and using.

V1.4.5 – Aug 26, 2016
# Updated WordPress Importer 0.6.3.
# Updated WooCommerce Template version.
# Fixed small CSS.
# Fixed break page when add many size product images.
# Updated sample.xml file.

V1.4.0 – Aug 10, 2016
# Updated Redux Famework.
# Updated WooCommerce Template version.
# Added Smooth Scrolling Feature.
# Fix small CSS.
# Optimization and fix Sticky Menu.
# Optimization SEO tags.

V1.3.8 – Jul 13, 2016
# Updated Redux Famework.
# Fix bug on shortcode blog.
#  Fix small CSS.
#  Improve One click demo data.

V1.3.7 – June 26, 2016
# Updated WooCommerce Template.

V1.3.5 – Mar 26, 2016
# Compatible with WordPress 4.5.
# Updated Visual Composer 4.11.2.1.

V1.3.4 – Mar 23, 2016
# Fixed malware.
# Small fixes.

V1.3.3 – Mar 16, 2016
# Upgrade Compatible WooCommerce 2.5.x.
# Small fixes.

V1.3.2 – Mar 08, 2016
# Updated : fixed error missing custom widget after updating theme.

V1.3.1 – Mar 03, 2016
# Upgrade Compatible WooCommerce 2.5.x.
# Fixed shipping in review order.
# Fixed price ajax load in review order.

V1.3 – Jan 15, 2016
# Fixed Ajax load image on product.
# Fixed choose color in Theme Option.
# Fixed line height submenu.
# Fixed show menu level 3.
# Changed Screenshot image.

V1.2.2 – Jan 06, 2016
# Updated Booking function.

V1.2.1 – Nov 11, 2015
# Improved One Click Demo Import.
# Updated Aqua Child Theme
# Small fixes

V1.2.0 – Oct 06, 2015
# Checked Compatible with: Visual Composer 4.7.4 
# Added Aqua Child Theme
# Small fixes

V1.2.0 – Sep 30, 2015
# Updated WooCommerce template files
# Updated plugins package
# small bugs Responsive
# Add 2 Homepages Version
# Add Single Product Page
# Add Cart Page
# Add Checkout Page

V1.0.0 – July 20, 2015
# Updated import function in Option Theme.

V1.0.0 – July 18, 2015
# Released