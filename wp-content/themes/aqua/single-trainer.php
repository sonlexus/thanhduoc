<?php get_header(); ?>
<?php
global $tb_options;
$tb_show_page_title = isset($tb_options['tb_post_show_page_title']) ? $tb_options['tb_post_show_page_title'] : 1;
$tb_show_page_breadcrumb = isset($tb_options['tb_post_show_page_breadcrumb']) ? $tb_options['tb_post_show_page_breadcrumb'] : 1;
tb_theme_title_bar($tb_show_page_title, $tb_show_page_breadcrumb);


$tb_trainer_single_content = (int) isset($tb_options['tb_trainer_single_content']) ? $tb_options['tb_trainer_single_content'] : '';
?>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<?php
				
					$cl_content = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
				?>
				<div class="<?php echo esc_attr($cl_content) ?> content tb-blog">
					<?php
					while ( have_posts() ) : the_post();
						$post_id = get_the_ID();
						get_template_part( 'framework/templates/trainer/single/entry', get_post_format());
						
					
					endwhile;
					?>
				</div>
				<!-- End Content -->
				<?php 
				if($tb_trainer_single_content){
					$page_data = get_page( $tb_trainer_single_content ); 
					$content = apply_filters('the_content', $page_data->post_content);
					echo $content;
				}
				?>
			</div>

		</div>
	</div>
<?php get_footer(); ?>