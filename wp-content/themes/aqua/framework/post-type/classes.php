<?php

// Register Custom Post Type

function jws_theme_add_post_type_classes() {

    // Register taxonomy

    $labels = array(

            'name'              => _x( 'Classes Category', 'taxonomy general name', 'aqua' ),

            'singular_name'     => _x( 'Classes Category', 'taxonomy singular name', 'aqua' ),

            'search_items'      => __( 'Search Classes Category', 'aqua' ),

            'all_items'         => __( 'All Classes Category', 'aqua' ),

            'parent_item'       => __( 'Parent Classes Category', 'aqua' ),

            'parent_item_colon' => __( 'Parent Classes Category:', 'aqua' ),

            'edit_item'         => __( 'Edit Classes Category', 'aqua' ),

            'update_item'       => __( 'Update Classes Category', 'aqua' ),

            'add_new_item'      => __( 'Add New Classes Category', 'aqua' ),

            'new_item_name'     => __( 'New Classes Category Name', 'aqua' ),

            'menu_name'         => __( 'Classes Category', 'aqua' ),

    );



    $args = array(

            'hierarchical'      => true,

            'labels'            => $labels,

            'show_ui'           => true,

            'show_admin_column' => true,

            'query_var'         => true,

            'rewrite'           => array( 'slug' => 'classes_category' ),

    );

    if(function_exists('custom_reg_taxonomy')) {

        custom_reg_taxonomy( 'classes_category', array( 'classes' ), $args );

    }
  

    //Register post type classes

    $labels = array(

            'name'                => _x( 'Classes', 'Post Type General Name', 'aqua' ),

            'singular_name'       => _x( 'Classes Item', 'Post Type Singular Name', 'aqua' ),

            'menu_name'           => __( 'Classes', 'aqua' ),

            'parent_item_colon'   => __( 'Parent Item:', 'aqua' ),

            'all_items'           => __( 'All Items', 'aqua' ),

            'view_item'           => __( 'View Item', 'aqua' ),

            'add_new_item'        => __( 'Add New Item', 'aqua' ),

            'add_new'             => __( 'Add New', 'aqua' ),

            'edit_item'           => __( 'Edit Item', 'aqua' ),

            'update_item'         => __( 'Update Item', 'aqua' ),

            'search_items'        => __( 'Search Item', 'aqua' ),

            'not_found'           => __( 'Not found', 'aqua' ),

            'not_found_in_trash'  => __( 'Not found in Trash', 'aqua' ),

    );

    $args = array(

            'label'               => __( 'Classes', 'aqua' ),

            'description'         => __( 'Classes Description', 'aqua' ),

            'labels'              => $labels,

            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),

            'taxonomies'          => array( 'classes_category', 'classes_tag' ),

            'hierarchical'        => true,

            'public'              => true,

            'show_ui'             => true,

            'show_in_menu'        => true,

            'show_in_nav_menus'   => true,

            'show_in_admin_bar'   => true,

            'menu_position'       => 5,

            'menu_icon'           => 'dashicons-pressthis',

            'can_export'          => true,

            'has_archive'         => true,

            'exclude_from_search' => false,

            'publicly_queryable'  => true,

            'capability_type'     => 'page',

    );

    

    if(function_exists('custom_reg_post_type')) {

        custom_reg_post_type( 'classes', $args );

    }

    

}



// Hook into the 'init' action

add_action( 'init', 'jws_theme_add_post_type_classes', 0 );

