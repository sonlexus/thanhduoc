<?php

// Register Custom Post Type

function jws_theme_add_post_type_events() {

    // Register taxonomy

    $labels = array(

            'name'              => _x( 'Events Category', 'taxonomy general name', 'aqua' ),

            'singular_name'     => _x( 'Events Category', 'taxonomy singular name', 'aqua' ),

            'search_items'      => __( 'Search Events Category', 'aqua' ),

            'all_items'         => __( 'All Events Category', 'aqua' ),

            'parent_item'       => __( 'Parent Events Category', 'aqua' ),

            'parent_item_colon' => __( 'Parent Events Category:', 'aqua' ),

            'edit_item'         => __( 'Edit Events Category', 'aqua' ),

            'update_item'       => __( 'Update Events Category', 'aqua' ),

            'add_new_item'      => __( 'Add New Events Category', 'aqua' ),

            'new_item_name'     => __( 'New Events Category Name', 'aqua' ),

            'menu_name'         => __( 'Events Category', 'aqua' ),

    );



    $args = array(

            'hierarchical'      => true,

            'labels'            => $labels,

            'show_ui'           => true,

            'show_admin_column' => true,

            'query_var'         => true,

            'rewrite'           => array( 'slug' => 'events_category' ),

    );

    if(function_exists('custom_reg_taxonomy')) {

        custom_reg_taxonomy( 'events_category', array( 'events' ), $args );

    }
  

    //Register post type events

    $labels = array(

            'name'                => _x( 'Events', 'Post Type General Name', 'aqua' ),

            'singular_name'       => _x( 'Events Item', 'Post Type Singular Name', 'aqua' ),

            'menu_name'           => __( 'Events', 'aqua' ),

            'parent_item_colon'   => __( 'Parent Item:', 'aqua' ),

            'all_items'           => __( 'All Items', 'aqua' ),

            'view_item'           => __( 'View Item', 'aqua' ),

            'add_new_item'        => __( 'Add New Item', 'aqua' ),

            'add_new'             => __( 'Add New', 'aqua' ),

            'edit_item'           => __( 'Edit Item', 'aqua' ),

            'update_item'         => __( 'Update Item', 'aqua' ),

            'search_items'        => __( 'Search Item', 'aqua' ),

            'not_found'           => __( 'Not found', 'aqua' ),

            'not_found_in_trash'  => __( 'Not found in Trash', 'aqua' ),

    );

    $args = array(

            'label'               => __( 'Events', 'aqua' ),

            'description'         => __( 'Events Description', 'aqua' ),

            'labels'              => $labels,

            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),

            'taxonomies'          => array( 'events_category', 'events_tag' ),

            'hierarchical'        => true,

            'public'              => true,

            'show_ui'             => true,

            'show_in_menu'        => true,

            'show_in_nav_menus'   => true,

            'show_in_admin_bar'   => true,

            'menu_position'       => 5,

            'menu_icon'           => 'dashicons-pressthis',

            'can_export'          => true,

            'has_archive'         => true,

            'exclude_from_search' => false,

            'publicly_queryable'  => true,

            'capability_type'     => 'page',

    );

    

    if(function_exists('custom_reg_post_type')) {

        custom_reg_post_type( 'events', $args );

    }

    

}



// Hook into the 'init' action

add_action( 'init', 'jws_theme_add_post_type_events', 0 );

