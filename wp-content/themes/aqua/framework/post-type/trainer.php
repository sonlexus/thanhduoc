<?php
	// Register Custom Post Type
	function jws_theme_add_post_type_trainer() {
		// Register taxonomy
		$labels = array(
		'name'              => _x( 'Trainer Category', 'taxonomy general name', 'preschool' ),
		'singular_name'     => _x( 'Trainer Category', 'taxonomy singular name', 'preschool' ),
		'search_items'      => __( 'Search Trainer Category', 'preschool' ),
		'all_items'         => __( 'All Trainer Category', 'preschool' ),
		'parent_item'       => __( 'Parent Trainer Category', 'preschool' ),
		'parent_item_colon' => __( 'Parent Trainer Category:', 'preschool' ),
		'edit_item'         => __( 'Edit Trainer Category', 'preschool' ),
		'update_item'       => __( 'Update Trainer Category', 'preschool' ),
		'add_new_item'      => __( 'Add New Trainer Category', 'preschool' ),
		'new_item_name'     => __( 'New Trainer Category Name', 'preschool' ),
		'menu_name'         => __( 'Trainer Category', 'preschool' ),
		);
		$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'trainer_category' ),
		);
		if(function_exists('custom_reg_taxonomy')) {
			custom_reg_taxonomy( 'trainer_category', array( 'trainer' ), $args );
		}
		//Register post type trainer
		$labels = array(
		'name'                => _x( 'Trainer', 'Post Type General Name', 'preschool' ),
		'singular_name'       => _x( 'Trainer Item', 'Post Type Singular Name', 'preschool' ),
		'menu_name'           => __( 'Trainer', 'preschool' ),
		'parent_item_colon'   => __( 'Parent Item:', 'preschool' ),
		'all_items'           => __( 'All Items', 'preschool' ),
		'view_item'           => __( 'View Item', 'preschool' ),
		'add_new_item'        => __( 'Add New Item', 'preschool' ),
		'add_new'             => __( 'Add New', 'preschool' ),
		'edit_item'           => __( 'Edit Item', 'preschool' ),
		'update_item'         => __( 'Update Item', 'preschool' ),
		'search_items'        => __( 'Search Item', 'preschool' ),
		'not_found'           => __( 'Not found', 'preschool' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'preschool' ),
		);
		$args = array(
		'label'               => __( 'Trainer', 'preschool' ),
		'description'         => __( 'Trainer Description', 'preschool' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'trainer_category', 'trainer_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-businessman',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		);
		if(function_exists('custom_reg_post_type')) {
			custom_reg_post_type( 'trainer', $args );
		}
	}
	// Hook into the 'init' action
add_action( 'init', 'jws_theme_add_post_type_trainer', 10 );