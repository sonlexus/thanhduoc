<?php
vc_map ( array (
	"name" => 'Blog Grid',
	"base" => "blog_grid",
	"icon" => "tb-icon-for-vc",
	"category" => __ ( 'Aqua', 'aqua' ), 
	'admin_enqueue_js' => array(URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'aqua'),
                "param_name" => "tpl",
                "value" => array(
					__("Template 1",'aqua') => "tpl1",
					__("Template 2 ~ Update",'aqua') => "tpl2",
					__("Template 3 ~ Update (one column)",'aqua') => "tpl3",
                ),
                "description" => __('Select template in this elment.', 'aqua')
            ),
		array (
				"type" => "tb_taxonomy",
				"taxonomy" => "category",
				"heading" => __ ( "Categories", 'aqua' ),
				"param_name" => "category",
				"description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
		),
		
		array (
				"type" => "textfield",
				"heading" => __ ( 'Count', 'aqua' ),
				"param_name" => "posts_per_page",
				'value' => '',
				"description" => __ ( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'aqua' )
		),
		array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Columns", 'aqua'),
				"param_name" => "columns",
				"value" => array(
					"4 Columns" => "4",
					"3 Columns" => "3",
					"2 Columns" => "2",
					"1 Column" => "1",
				),
				'dependency' => array(
					'element' => 'tpl',
					'value'  => array('tpl1','tpl2'),
				),
				"description" => __('Select columns display in this element.', 'aqua')
		),
		array (
				"type" => "dropdown",
				"heading" => __ ( 'Order by', 'aqua' ),
				"param_name" => "orderby",
				"value" => array (
						"None" => "none",
						"Title" => "title",
						"Date" => "date",
						"ID" => "ID"
				),
				"description" => __ ( 'Order by ("none", "title", "date", "ID").', 'aqua' )
		),
		array (
				"type" => "dropdown",
				"heading" => __ ( 'Order', 'aqua' ),
				"param_name" => "order",
				"value" => Array (
						"None" => "none",
						"ASC" => "ASC",
						"DESC" => "DESC"
				),
				"description" => __ ( 'Order ("None", "Asc", "Desc").', 'aqua' )
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'aqua'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Image", 'aqua'),
			"param_name" => "show_image",
			"value" => array (
				__ ( "Yes, please", 'aqua' ) => true
			),
			"dependency" => array(
				'element' => 'tpl',
				'value' => 'tpl1', 
			),
			"group" => __("Template", 'aqua'),
			"description" => __("Show or not image of post in this element.", 'aqua')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Title", 'aqua'),
			"param_name" => "show_title",
			"value" => array (
				__ ( "Yes, please", 'aqua' ) => true
			),
			"group" => __("Template", 'aqua'),
			"description" => __("Show or not title of post in this element.", 'aqua')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Info", 'aqua'),
			"param_name" => "show_info",
			"value" => array (
				__ ( "Yes, please", 'aqua' ) => true
			),
			"group" => __("Template", 'aqua'),
			"description" => __("Show or not info of post in this element.", 'aqua')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Excerpt", 'aqua'),
			"param_name" => "show_excerpt",
			"value" => array (
				__ ( "Yes, please", 'aqua' ) => true
			),
			'std' => 18,
			"group" => __("Template", 'aqua'),
			"description" => __("Show or not excerpt of post in this element.", 'aqua')
		),

		array(
			"type" => "textfield",
			"heading" => __('Excerpt Length', "aqua"),
			"param_name" => "excerpt_length",
			"value" => '',
			"group" => __("Template", 'aqua'),
			"description" => __('The length of the excerpt, number of words to display. Set -1 show all words of excerpt.', "aqua")
		),
		array(
			"type" => "textfield",
			"heading" => __('Excerpt More', "aqua"),
			"param_name" => "excerpt_more",
			"value" => "",
			"group" => __("Template", 'aqua'),
			"description" => __('Please enter excerpt more for blog.', "aqua")
		),
		  array(
                "type" => "checkbox", 
                "heading" => __('Crop image', "aqua"),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", "aqua") => 1
                ),
                "description" => __('Crop or not crop image on your Post.', "aqua")
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', "aqua"),
                "param_name" => "width_image",
				
                "description" => __('Enter the width of image. Default: 584.', "aqua")
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', "aqua"),
                "param_name" => "height_image",
                "description" => __('Enter the height of image. Default: 232.', "aqua")
            ),
	)
));