<?php

add_action('init', 'tb_trainer_integrateWithVC');

function tb_trainer_integrateWithVC() {
    vc_map(array(
        "name" => __("Trainer", 'aqua'),
        "base" => "tb_trainer",
        "class" => "tb-trainer",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
            array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Template", 'aqua'),
				"param_name" => "tpl",
				"value" => array(
					__("Template 1 ~ Yoga Trainers",'aqua') => "tpl1",
					__("Template 2 ~ Fitness Trainers",'aqua') => "tpl2",
				),
				"std" => 'tpl1',
				"group" => __("Template", 'aqua'),
				"description" => __('Select template in this element.', 'aqua')
			),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", 'aqua'),
                "param_name" => "posts_per_page",
                "value" => "",
				"description" => __('Please, enter number of post per page for trainer. Show all: -1.', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    "4 Columns" => "4",
                    "3 Columns" => "3",
                    "2 Columns" => "2",
                    "1 Column" => "1",
                ),
				"description" => __('Select columns for trainer.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"std" => 1,
				"group" => __("Build Query", 'trainer'),
                "description" => __('Show or hide title of post on your trainer.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Position', 'aqua'),
                "param_name" => "show_position",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"std" => 1,
				"group" => __("Build Query", 'trainer'),
                "description" => __('Show or hide info of post on your trainer.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Socials', 'aqua'),
                "param_name" => "show_social",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"std" => 1,
				'dependency' => array(
					'element'  => 'tpl',
					'value' => 'tpl2',
				),
				"group" => __("Build Query", 'trainer'),
                "description" => __('Show or hide socials of post on your trainer.', 'aqua')
            ),
			array (

				"type" => "tb_taxonomy",

				"taxonomy" => "trainer_category",

				"heading" => __( "Trainer Categories", 'aqua' ),

				"param_name" => "category",
				
				"group" => __("Template", 'aqua'),
				
				"description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )

			),
			 array(
                "type" => "checkbox", 
                "heading" => __('Crop image', 'aqua'),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
				"group" => __("Template", 'aqua'),
                "description" => __('Crop or not crop image on your Post.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', 'aqua'),
                "param_name" => "width_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the width of image. Default: 300.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', 'aqua'),
                "param_name" => "height_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the height of image. Default: 200.', 'aqua')
            ),
			 array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'trainer' ),
				"param_name" => "orderby",
				"value" => array (
						__("None",'trainer') => "none",
						__("Date",'trainer') => "date",
						__("Price",'trainer') => "price",
						__("Random",'trainer') => "rand",
						__("Selling",'trainer') => "selling",
						__("Rated",'trainer') => "rated",
				),
				"group" => __("Build Query", 'trainer'),
				"description" => __( 'Order by ("none", "date", "price", "rand", "selling", "rated") in this element.', 'trainer' )
			),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'trainer'),
                "param_name" => "order",
                "value" => Array(
                    __("None",'trainer') => "none",
                    __("ASC",'trainer') => "ASC",
                    __("DESC",'trainer') => "DESC"
                ),
				"group" => __("Build Query", 'trainer'),
                "description" => __('Order ("None", "Asc", "Desc") in this element.', 'trainer')
            ),
			
			 array(
                "type" => "checkbox",
                "heading" => __('Show Pagination', 'aqua'),
                "param_name" => "show_pagination",
                 "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 0,
                "description" => __('Show or hide pagination of post on your classes.', 'aqua')
            ),
			array(
                "type" => "dropdown",
                "heading" => __('Pagination Position', 'aqua'),
                "param_name" => "pos_pagination",
                "value" => Array(
                    "Left" => "text-left",
                    "Center" => "text-center",
					"Right" => "text-right"
                ),
				'std' => "text-center",
                "description" => __('Select Pagination Position.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
