<?php
add_action('init', 'tb_events_carousel_integrateWithVC');

function tb_events_carousel_integrateWithVC() {
    vc_map(array(
        "name" => __("Events carousel", 'aqua'),
        "base" => "tb_events_carousel",
        "class" => "tb-events-carousel",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", 'aqua'),
                "param_name" => "posts_per_page",
                "value" => "",
				"description" => __('Please, enter number of post per page for events. Show all: -1.', 'aqua')
            ),
            array (
                "type" => "tb_taxonomy",
                "taxonomy" => "events_category",
                "heading" => __ ( "Categories", 'aqua' ),
                "param_name" => "events_category",
                "class" => "",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    "4 Columns" => "4",
                    "3 Columns" => "3",
                    "2 Columns" => "2",
                    "1 Column" => "1",
                ),
				"description" => __('Select columns for events.', 'aqua')
            ),
			
            array(
                "type" => "checkbox", 
                "heading" => __('Crop image', 'aqua'),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"group" => __("Template", 'aqua'),
                "description" => __('Crop or not crop image on your Post.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', 'aqua'),
                "param_name" => "width_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the width of image. Default: 300.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', 'aqua'),
                "param_name" => "height_image",
				"group" => __("Template", 'aqua'),
                "description" => __('Enter the height of image. Default: 200.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Time', 'aqua'),
                "param_name" => "show_time",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide categories of post on your events.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide title of post on your events.', 'aqua')
            ),
			
            array(
                "type" => "checkbox",
                "heading" => __('Show Address', 'aqua'),
                "param_name" => "show_address",
				"group" => __("Template", 'aqua'),
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				'std' => 1,
                "description" => __('Show or hide info of post on your events.', 'aqua')
            ),
           
            array(
                "type" => "dropdown",
                "heading" => __('Order by', 'aqua'),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'aqua'),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', 'aqua')
            ),
           
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __ ( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
        )
    ));
}
