<?php
if(class_exists('Woocommerce')){
    vc_map(array(
        "name" => __("Product Carousel", 'aqua'),
        "base" => "tb-products-carousel",
        "class" => "tb-products-carousel",
        "category" => __('Aqua', 'aqua'),
        'admin_enqueue_js' => array(URI_PATH_ADMIN.'/assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'aqua'),
                "param_name" => "tpl",
                "value" => array(
					__("Template 1",'aqua') => "tpl1",
					__("Template 2",'aqua') => "tpl2",
					__("Template 3",'aqua') => "tpl3",
					__("Template 4",'aqua') => "tpl4",
                ),
                "description" => __('Select template in this elment.', 'aqua')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'aqua'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'aqua' )
            ),
			array (
                "type" => "tb_taxonomy",
                "taxonomy" => "product_cat",
                "heading" => __ ( "Categories", 'aqua' ),
                "param_name" => "product_cat",
                "class" => "",
				"group" => __("Build Query", 'aqua'),
                "description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'aqua' )
            ),
			array (
					"type" => "dropdown",
					"class" => "",
					"heading" => __( "Show", 'aqua' ),
					"param_name" => "show",
					"value" => array (
							__("All Products",'aqua') => "all_products",
							__("Featured Products",'aqua') => "featured",
							__("On-sale Products",'aqua') => "onsale",
                            __("Variable Products",'aqua') => "variable"
					),
					"group" => __("Build Query", 'aqua'),
					"description" => __( "Select show product type in this elment", 'aqua' )
			),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Product Count", 'aqua'),
                "param_name" => "number",
                "value" => "",
				"group" => __("Build Query", 'aqua'),
				"description" => __('Please, enter number of post per page. Show all: -1.', 'aqua')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'aqua'),
                "param_name" => "columns",
                "value" => array(
                    __("4 Columns",'aqua') => "4",
                    __("3 Columns",'aqua') => "3",
                    __("2 Columns",'aqua') => "2",
                    __("1 Column",'aqua') => "1",
                ),
                "description" => __('Select columns in this elment.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Hide Free', 'aqua'),
                "param_name" => "hide_free",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"group" => __("Build Query", 'aqua'),
                "description" => __('Hide free product in this element.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Hidden', 'aqua'),
                "param_name" => "show_hidden",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
				"group" => __("Build Query", 'aqua'),
                "description" => __('Show Hidden product in this element.', 'aqua')
            ),
            array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'aqua' ),
				"param_name" => "orderby",
				"value" => array (
						__("None",'aqua') => "none",
						__("Date",'aqua') => "date",
						__("Price",'aqua') => "price",
						__("Random",'aqua') => "rand",
						__("Selling",'aqua') => "selling",
						__("Rated",'aqua') => "rated",
				),
				"group" => __("Build Query", 'aqua'),
				"description" => __( 'Order by ("none", "date", "price", "rand", "selling", "rated") in this element.', 'aqua' )
			),
			array(
                "type" => "checkbox", 
                "heading" => __('Crop image', "aqua"),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", "aqua") => 1
                ),
                "description" => __('Crop or not crop image on your Post.', "aqua")
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', "aqua"),
                "param_name" => "width_image",
				
                "description" => __('Enter the width of image. Default: 370.', "aqua")
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', "aqua"),
                "param_name" => "height_image",
                "description" => __('Enter the height of image. Default: 270.', "aqua")
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'aqua'),
                "param_name" => "order",
                "value" => Array(
                    __("None",'aqua') => "none",
                   __("ASC",'aqua') => "ASC",
                    __("DESC",'aqua') => "DESC"
                ),
				"group" => __("Build Query", 'aqua'),
                "description" => __('Order ("None", "Asc", "Desc") in this element.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Sale Flash', 'aqua'),
                "param_name" => "show_sale_flash",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide sale flash of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'aqua'),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide title of product.', 'aqua')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Product categories', 'aqua'),
                "param_name" => "show_cat",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
                "group" => __("Template", 'aqua'),
                "description" => __('Show or hide title of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Price', 'aqua'),
                "param_name" => "show_price",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide price of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Rating', 'aqua'),
                "param_name" => "show_rating",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide rating of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Add To Cart', 'aqua'),
                "param_name" => "show_add_to_cart",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide add to cart of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Quick View', 'aqua'),
                "param_name" => "show_quick_view",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide quick view of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Whishlist', 'aqua'),
                "param_name" => "show_whishlist",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide whishlish of product.', 'aqua')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Compare', 'aqua'),
                "param_name" => "show_compare",
                "value" => array(
                    __("Yes, please", 'aqua') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'aqua'),
                "description" => __('Show or hide compare of product.', 'aqua')
            ),
        )
    ));
}
