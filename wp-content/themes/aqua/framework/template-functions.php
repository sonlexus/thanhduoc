<?php
	if ( ! isset( $content_width ) ) $content_width = 900;
	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
	if ( ! function_exists( 'tb_theme_setup' ) ) {
		function tb_theme_setup() {
			load_theme_textdomain( 'aqua', get_template_directory() . '/languages' );
			// Add Custom Header.
			add_theme_support('custom-header');
			// Add RSS feed links to <head> for posts and comments.
			add_theme_support( 'automatic-feed-links' );
			// Enable support for Post Thumbnails, and declare two sizes.
			add_theme_support( 'post-thumbnails' );
			// This theme uses wp_nav_menu() in two locations.
			register_nav_menus( array(
			'main_navigation'   => __( 'Main Navigation','aqua' ),
			) );
			/*
				* Switch default core markup for search form, comment form, and comments
				* to output valid HTML5.
			*/
			add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
			) );
			/*
				* Enable support for Post Formats.
				* See http://codex.wordpress.org/Post_Formats
			*/
			add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery','status'
			) );
			// This theme allows users to set a custom background.
			add_theme_support( 'custom-background', apply_filters( 'tbtheme_custom_background_args', array(
			'default-color' => 'f5f5f5',
			) ) );
			// Add support for featured content.
			add_theme_support( 'featured-content', array(
			'featured_content_filter' => 'tbtheme_get_featured_posts',
			'max_posts' => 6,
			) );
			add_theme_support( "title-tag" );
			// This theme uses its own gallery styles.
			
			add_image_size( 'gallery-big-thumb', 576, 350, true );
			add_image_size( 'gallery-thumb', 384, 350, true );
			add_image_size( 'gallery-widget-thumbnail', 90, 107, true );
			add_image_size( 'instagram-widget-thumbnail', 148, 108, true );
			
			add_image_size( 'gallery-horizontal-thumb', 384, 480, true );
			add_image_size( 'gallery-vertical-thumb', 576, 350, true );
			add_image_size( 'gallery-normal-thumb', 384, 350, true );
			add_image_size( 'gallery-small-thumb', 384, 220, true );
			add_image_size( 'gallery_small_thumb_2', 150, 110, true );
			
			add_image_size( 'portfolio-horizontal-thumb', 470, 260, true );
			add_image_size( 'portfolio-vertical-thumb', 340, 530, true );
			add_image_size( 'portfolio-normal-thumb', 230, 260, true );
			
			add_filter( 'use_default_gallery_style', '__return_false' );
		}
	}
	add_action( 'after_setup_theme', 'tb_theme_setup' );
	add_filter( 'post-format-status', 'tb_avia_status_content_filter', 10, 1 );
	if(!function_exists('tb_avia_status_content_filter'))
	{
		function tb_avia_status_content_filter($current_post)
		{
			/* FUNCTION HERE */
		}
	}
	/* Favicon */
	if (!function_exists('tb_theme_favicon')) {
		function tb_theme_favicon() {
			global $tb_options;
			$icon = $tb_options['tb_favicon_image']['url'] ? $tb_options['tb_favicon_image']['url']: URI_PATH.'/favicon.ico';
			echo '<link rel="shortcut icon" href="' . esc_url($icon) . '"/>';
		}
	}
	add_action('wp_head', 'tb_theme_favicon');
	/* Favicon */
	if (!function_exists('tb_theme_logo')) {
		function tb_theme_logo() {
			global $tb_options,$post;
			$postid = isset($post->ID)?$post->ID:0;
			$logo = isset($tb_options['tb_logo_image']['url']) ? $tb_options['tb_logo_image']['url'] : URI_PATH.'/assets/images/logo.png';
			$logo = get_post_meta($postid, 'tb_custom_logo', true) ? get_post_meta($postid, 'tb_custom_logo', true):$logo;
			$tb_sub_logo = get_post_meta($postid, 'tb_sub_logo', true) ? get_post_meta($postid, 'tb_sub_logo', true):'';
			echo '<img src="'.esc_url($logo).'" alt="Logo"/>';
			if($tb_sub_logo):
			echo '<img src="'.esc_url($tb_sub_logo).'" alt="Logo"/>';		
			endif;
		}
	}
	/* Custom Site Title */
	if (!function_exists('tb_theme_wp_title')) {
		function tb_theme_wp_title( $title, $sep ) {
			global $paged, $page;
			if ( is_feed() ) {
				return $title;
			}
			// Add the site description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) ) {
				$title = "$title $sep $site_description";
			}
			// Add a page number if necessary.
			if ( $paged >= 2 || $page >= 2 ) {
				$title = "$title $sep " . sprintf( __( 'Page %s', 'aqua' ), max( $paged, $page ) );
			}
			return $title;
		}
	}
	add_filter( 'wp_title', 'tb_theme_wp_title', 10, 2 );
	/* Page title */
	if (!function_exists('tb_theme_page_title')) {
		 function tb_theme_page_title() { 
            ob_start();
			if( is_home() ){
				_e('Home', 'aqua');
			}elseif(is_search()){
                _e('Search Keyword: ', 'aqua');
				echo '<span class="keywork">'. get_search_query(). '</span>';
            }elseif (!is_archive()) {
                the_title();
            } else { 
                if (is_category()){
                    single_cat_title();
                }elseif(get_post_type() == 'recipe' || get_post_type() == 'portfolio' || get_post_type() == 'produce' || get_post_type() == 'team' || get_post_type() == 'testimonial' || get_post_type() == 'myclients' || get_post_type() == 'product'){
                    single_term_title();
                }elseif (is_tag()){
                    single_tag_title();
                }elseif (is_author()){
                    printf(__('Author: %s', 'aqua'), '<span class="vcard">' . get_the_author() . '</span>');
                }elseif (is_day()){
                    printf(__('Day: %s', 'aqua'), '<span>' . get_the_date() . '</span>');
                }elseif (is_month()){
                    printf(__('Month: %s', 'aqua'), '<span>' . get_the_date('F Y') . '</span>');
                }elseif (is_year()){
                    printf(__('Year: %s', 'aqua'), '<span>' . get_the_date('Y') . '</span>');
                }elseif (is_tax('post_format', 'post-format-aside')){
                    _e('Asides', 'aqua');
                }elseif (is_tax('post_format', 'post-format-gallery')){
                    _e('Galleries', 'aqua');
                }elseif (is_tax('post_format', 'post-format-image')){
                    _e('Images', 'aqua');
                }elseif (is_tax('post_format', 'post-format-video')){
                    _e('Videos', 'aqua');
                }elseif (is_tax('post_format', 'post-format-quote')){
                    _e('Quotes', 'aqua');
                }elseif (is_tax('post_format', 'post-format-link')){
                    _e('Links', 'aqua');
                }elseif (is_tax('post_format', 'post-format-status')){
                    _e('Statuses', 'aqua');
                }elseif (is_tax('post_format', 'post-format-audio')){
                    _e('Audios', 'aqua');
                }elseif (is_tax('post_format', 'post-format-chat')){
                    _e('Chats', 'aqua');
                }else{
                    _e('Archives', 'aqua');
                }
            }
                
            return ob_get_clean();
		}
	}
	/* Page breadcrumb */
	if (!function_exists('tb_theme_page_breadcrumb')) {
		function tb_theme_page_breadcrumb($delimiter) {
			ob_start();
			$delimiter = esc_attr($delimiter);
			$home = __('Home', 'aqua');
			$before = '<span class="current">'; // tag before the current crumb
			$after = '</span>'; // tag after the current crumb
			global $post;
			$homeLink = home_url();
			if( is_home() ){
				_e('Home', 'aqua');
				}else{
				echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
			}
			if ( is_category() ) {
				$thisCat = get_category(get_query_var('cat'), false);
				if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
				echo tb_filtercontent($before . __('Archive by category: ', 'aqua') . single_cat_title('', false) . $after);
				} elseif ( is_search() ) {
				echo tb_filtercontent($before . __('Search results for: ', 'aqua') . get_search_query() . $after);
				} elseif ( is_day() ) {
				echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F').' '. get_the_time('Y') . '</a> ' . $delimiter . ' ';
				echo tb_filtercontent($before . get_the_time('d') . $after);
				} elseif ( is_month() ) {
				echo tb_filtercontent($before . get_the_time('F'). ' '. get_the_time('Y') . $after);
				} elseif ( is_single() && !is_attachment() ) {
				if ( get_post_type() != 'post' ) {
					if(get_post_type() == 'portfolio'){
						$terms = get_the_terms(get_the_ID(), 'portfolio_category', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'portfolio_category', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'recipe'){
						$terms = get_the_terms(get_the_ID(), 'recipe_category', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'recipe_category', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'produce'){
						$terms = get_the_terms(get_the_ID(), 'produce_category', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'produce_category', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'team'){
						$terms = get_the_terms(get_the_ID(), 'team_category', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'team_category', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'testimonial'){
						$terms = get_the_terms(get_the_ID(), 'testimonial_category', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'testimonial_category', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'myclients'){
						$terms = get_the_terms(get_the_ID(), 'clientscategory', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'clientscategory', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}elseif(get_post_type() == 'product'){
						$terms = get_the_terms(get_the_ID(), 'product_cat', '' , '' );
						if($terms) {
							the_terms(get_the_ID(), 'product_cat', '' , ', ' );
							echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
							}else{
							echo tb_filtercontent($before . get_the_title() . $after);
						}
						}else{
						$post_type = get_post_type_object(get_post_type());
						$slug = $post_type->rewrite;
						echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
						echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
					}
					} else {
					$cat = get_the_category(); $cat = $cat[0];
					$cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
					echo tb_filtercontent($cats);
					echo tb_filtercontent($before . get_the_title() . $after);
				}
				} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
				$post_type = get_post_type_object(get_post_type());
				if($post_type) echo tb_filtercontent($before . $post_type->labels->singular_name . $after);
				} elseif ( is_attachment() ) {
				$parent = get_post($post->post_parent);
				$cat = get_the_category($parent->ID); $cat = $cat[0];
				echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
				echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				} elseif ( is_page() && !$post->post_parent ) {
				echo tb_filtercontent($before . get_the_title() . $after);
				} elseif ( is_page() && $post->post_parent ) {
				$parent_id  = $post->post_parent;
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo tb_filtercontent($breadcrumbs[$i]);
					if ($i != count($breadcrumbs) - 1)
					echo ' ' . $delimiter . ' ';
				}
				echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				} elseif ( is_tag() ) {
				echo tb_filtercontent($before . __('Posts tagged: ', 'aqua') . single_tag_title('', false) . $after);
				} elseif ( is_author() ) {
				global $author;
				$userdata = get_userdata($author);
				echo tb_filtercontent($before . __('Articles posted by ', 'aqua') . $userdata->display_name . $after);
				} elseif ( is_404() ) {
				echo tb_filtercontent($before . __('Error 404', 'aqua') . $after);
			}
			if ( get_query_var('paged') ) {
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
				echo ' '.$delimiter.' '.__('Page', 'aqua') . ' ' . get_query_var('paged');
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
			}
			return ob_get_clean();
		}
	}
	/* Custom excerpt */
	function tb_custom_excerpt($limit, $more) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt) >= $limit || $limit != -1) {
			array_pop($excerpt);
			$excerpt = implode(" ", $excerpt) . $more;
			} else {
			$excerpt = implode(" ", $excerpt);
		}
		$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
		return $excerpt;
	}
	/* Display navigation to next/previous set of posts */
	if ( ! function_exists( 'tb_theme_paging_nav' ) ) {
		function tb_theme_paging_nav() {
			if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
				return;
			}
			$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
			$pagenum_link = html_entity_decode( get_pagenum_link() );
			$query_args   = array();
			$url_parts    = explode( '?', $pagenum_link );
			if ( isset( $url_parts[1] ) ) {
				wp_parse_str( $url_parts[1], $query_args );
			}
			$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
			$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
			$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
			$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
			// Set up paginated links.
			$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $GLOBALS['wp_query']->max_num_pages,
			'current'  => $paged,
			'mid_size' => 1,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => __( '<i class="fa fa-angle-left"></i>&nbsp; Previous', 'aqua' ),
			'next_text' => __( 'Next &nbsp;<i class="fa fa-angle-right"></i>', 'aqua' ),
			) );
			if ( $links ) {
			?>
			<nav class="pagination text-right" role="navigation">
				<?php echo tb_filtercontent($links); ?>
			</nav>
			<?php
			}
		}
	}
	/* Display navigation to next/previous post */
	if ( ! function_exists( 'tb_theme_post_nav' ) ) {
		function tb_theme_post_nav() {
			$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
			$next     = get_adjacent_post( false, '', false );
			if ( ! $next && ! $previous ) {
				return;
			}
		?>
		<nav class="navigation post-navigation clearfix" role="navigation">
			<div class="nav-links">
				<?php
					previous_post_link( '<div class="nav-previous">%link</div>', __( '<span class="btn btn-default"><i class="fa fa-angle-left"></i>&nbsp; Previous</span>', 'aqua' ) );
					next_post_link(     '<div class="nav-next">%link</div>',     __( '<span class="btn btn-default">Next &nbsp;<i class="fa fa-angle-right"></i></span>', 'aqua' ) );
				?>
			</div>
		</nav>
		<?php
		}
	}
	/* Title Bar */
	if ( ! function_exists( 'tb_theme_title_bar' ) ) {
		function tb_theme_title_bar($tb_show_page_title, $tb_show_page_breadcrumb) {
			global $tb_options;
			$delimiter = isset($tb_options['tb_page_breadcrumb_delimiter']) ? $tb_options['tb_page_breadcrumb_delimiter'] : '/';
			$cl_page_title = $cl_page_breadcrumb = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
			$tpl = isset($tb_options['tb_title_bar_layout']) ? $tb_options['tb_title_bar_layout'] : 'tpl1';
			$class = array();
			$class[] = 'title-bar';
			$class[] = $tpl;
			if($tpl == 'tpl1'){
				if($tb_show_page_title && $tb_show_page_breadcrumb){
					$cl_page_title = $cl_page_breadcrumb = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
					}else{
					if($tb_show_page_title){
						$cl_page_title = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
					}
					if($tb_show_page_breadcrumb){
						$cl_page_breadcrumb = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
					}
				}
			}
			if($tb_show_page_title || $tb_show_page_breadcrumb){ 
			?>
			<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
				<div class="container container-height">
					<div class="row row-height">
						<?php if($tb_show_page_title){ ?>
							<div class="<?php echo esc_attr($cl_page_title); ?> col-height col-middle">
								<h1 class="page-title"><?php echo tb_theme_page_title(); ?></h1>
							</div>
						<?php } ?>
						<?php if($tb_show_page_breadcrumb){ ?>
							<div class="<?php echo esc_attr($cl_page_breadcrumb); ?> col-height col-middle">
								<div class="page-breadcrumb"><?php echo tb_theme_page_breadcrumb($delimiter); ?></div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php 
			}
		}
	}
	/*Custom comment list*/
	function tb_custom_comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
			} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
	?>
	<<?php echo tb_filtercontent($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
			<?php endif; ?>
			<div class="comment-avatar">
				<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			</div>
			<div class="comment-info">
				<div class="comment-author vcard">
					<?php printf( __( '<h4>%s</h4>','aqua' ), get_comment_author_link() ); ?>
				</div>
				<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'aqua' ); ?></em>
				<br />
				<?php endif; ?>
				<?php comment_text(); ?>
				<div class="comment-footer">
					<div class="comment-meta commentmetadata">
						<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
							<?php
								/* translators: 1: date, 2: time */
							printf( __('%1$s at %2$s','aqua'), get_comment_date(),  get_comment_time() ); ?>
						</a>
						<?php edit_comment_link( __( '(Edit)', 'aqua' ), '  ', '' ); ?>
					</div>
					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div>
				</div>
			</div>
			<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
		<?php
		}
		function tb_addURLParameter($url, $paramName, $paramValue) {
			$url_data = parse_url($url);
			if(!isset($url_data["query"]))
			$url_data["query"]="";
			$params = array();
			parse_str($url_data['query'], $params);
			$params[$paramName] = $paramValue;
			if( $paramName == 'product_count' ) {
				$params['paged'] = '1';
			}
			$url_data['query'] = http_build_query($params);
			return tb_build_url($url_data);
		}
		function tb_build_url($url_data) {
			$url="";
			if(isset($url_data['host']))
			{
				$url .= $url_data['scheme'] . '://';
				if (isset($url_data['user'])) {
					$url .= $url_data['user'];
					if (isset($url_data['pass'])) {
						$url .= ':' . $url_data['pass'];
					}
					$url .= '@';
				}
				$url .= $url_data['host'];
				if (isset($url_data['port'])) {
					$url .= ':' . $url_data['port'];
				}
			}
			if (isset($url_data['path'])) {
				$url .= $url_data['path'];
			}
			if (isset($url_data['query'])) {
				$url .= '?' . $url_data['query'];
			}
			if (isset($url_data['fragment'])) {
				$url .= '#' . $url_data['fragment'];
			}
			return $url;
		}
		/* Filter Format Currencty position */
		add_filter('woocommerce_price_format','tb_woocommerce_price_format', 10, 2);
		function tb_woocommerce_price_format($format, $currency_pos){
			$currency_pos = get_option( 'woocommerce_currency_pos' );
			if(!$currency_pos):
			update_option( 'woocommerce_currency_pos', 'left' );
			endif;
		}	
function tb_get_trainer(){
	$args = array(
        'post_type' => 'trainer',
        'post_status' => 'publish'
	);
	$name = array();
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) {
		while ( $wp_query->have_posts() ) { $wp_query->the_post();
				$name[] = get_the_title();
		}
	}
	return $name;
}		
function tb_get_image_width( $size ) {
	global $_wp_additional_image_sizes;
	if ( in_array( $size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
		return get_option( "{$size}_size_w" );
		} elseif ( isset( $_wp_additional_image_sizes[ $size ] ) ) {
		return $_wp_additional_image_sizes[ $size ]['width'];
	}
	return false;
}		
function tb_get_image_height( $size ) {
	global $_wp_additional_image_sizes;
	if ( in_array( $size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
		return get_option( "{$size}_size_h" );
		} elseif ( isset( $_wp_additional_image_sizes[ $size ] ) ) {
		return $_wp_additional_image_sizes[ $size ]['height'];
	}
	return false;
}
function tb_footer() {
	global $tb_options,$post;
		$footer_layout = $tb_options["tb_footer_layout"];
		if($post){
			$tb_footer = get_post_meta($post->ID, 'tb_footer', true)?get_post_meta($post->ID, 'tb_footer', true):'global';
			$footer_layout = $tb_footer=='global'?$footer_layout:$tb_footer;
			//echo $footer_layout;
		}
		switch ($footer_layout) {
		case 'v1':
			get_template_part('framework/footers/footer', 'v1');
			break;
		case 'v2':
			get_template_part('framework/footers/footer', 'v2');
			break;
		case 'v3':
			get_template_part('framework/footers/footer', 'v3');
			break;
		case 'v4':
			get_template_part('framework/footers/footer', 'v4');
			break;
		case 'v5':
			get_template_part('framework/footers/footer', 'v5');
			break;
		case 'white':
			get_template_part('framework/footers/footer', 'white');
			break;
		default :
			get_template_part('framework/footers/footer', 'v2');
			break;
	}
}

function tb_get_page_id( $id, $global ){
	global $tb_options;
	if( is_archive() || is_search() || is_single() ){
		if( isset( $tb_options[ $id ] ) ){
			return $tb_options[ $id ];
		}else{
			$post_id = get_option('page_on_front');
		}
	}else{
		$post_id = get_queried_object_id();
	}

	$result = get_post_meta( $post_id , $id, true );

	if( $global && ($result=='global'||$result=='') && isset( $tb_options[ $id ] ) ){
		return $tb_options[ $id ];
	}
	return $result;
}

function tb_get_object_id( $id, $global=false ){
	if( function_exists('is_woocommerce') && is_woocommerce() )
		return tb_get_shop_id( $id, $global );
	else
		return tb_get_page_id( $id, $global );
}
function tb_time(){
	$option = array();
	for($i = 7 ; $i<= 20 ; $i++){
		$option[] = $i .'=>'. $i .':00';
	}
	return $option;
}


add_action( 'wp_ajax_tb_render_schedule_table', 'tb_render_schedule_table' );
add_action( 'wp_ajax_nopriv_tb_render_schedule_table', 'tb_render_schedule_table' );

function tb_render_schedule_table(){
	global $wpdb, $pagenow;
	$data = (array)$_POST['data'];
	
	//if( empty( $data['args']) || empty( $data['atts']) ) return;
	
	$query_args = (array)$data['args'];
	
	$atts =  wp_parse_args( (array)$data['atts'], array(
		'begin' => 7,
		'end' => 20,
		'datestart' => '2017-01-13',
		'dateend' => '2017-01-25',
        'el_class' => ''

     ) );
	extract( $atts );
	
    if( isset( $data['datestart'] ) && $data['dateend'] ){
    	$datestart = $data['datestart'];
    	$dateend = $data['dateend'];
    }
	
	$class = array();
	
	$dayofweek = array(
		'' => '&nbsp;',
		'monday' => esc_html__('Monday', 'aqua'),
		'tuesday' => esc_html__('Tuesday', 'aqua'),
		'wednesday' => esc_html__('Wednesday', 'aqua'),
		'thurday' => esc_html__('Thurday', 'aqua'),
		'friday' => esc_html__('Friday', 'aqua'),
		'saturday' => esc_html__('Saturday', 'aqua'),
		'sunday' => esc_html__('Sunday', 'aqua'),
	);
	$time_array = array();
	for($begin; $begin<= $end; $begin++){
		$time_array[$begin] = $begin .':00';
	}
	
	$query_args = array(
		'post_type' => 'classes',
		'post_status' => 'publish'
	);

	 ob_start();
	?>
		<?php echo 'zozozo'; ?>
	<?php
	wp_reset_postdata();
	$response['content'] = ob_get_clean();
	echo json_encode( $response );
	wp_die();
}
add_action( 'wp_ajax_tb_render_cal_body', 'tb_render_cal_body' );
add_action( 'wp_ajax_nopriv_tb_render_cal_body', 'tb_render_cal_body' );

function tb_render_cal_body(){
	global $wpdb, $pagenow;
	$data = (array)$_POST['data'];

    if( isset( $data['bust'] ) && isset( $data['hips'] ) && isset( $data['waist'])){
    	$bust = $data['bust'];
    	$waist = $data['waist'];
    	$hips = $data['hips'];
		$body_shape = '';
			
		$bustMeasure   = "";
		$waistMeasure  = "";
		$hipsMeasure   = "";

		$small   = "small";
		$medium  = "medium";
		$large   = "large";

		$shapeType = "";

		if ( $bust <= 36 ) { $bustMeasure = $small;  }
		if ( $bust <= 44 ) { $bustMeasure = $medium; }
		if ( $bust >= 45 ) { $bustMeasure = $large;  }
		
		if ( $waist <= 34 ) { $waistMeasure = $small;  }
		if ( $waist <= 41 ) { $waistMeasure = $medium; }
		if ( $waist >= 42 ) { $waistMeasure = $large;  }

		if ( $hips <= 39 ) { $hipsMeasure = $small;  }
		if ( $hips <= 47 ) { $hipsMeasure = $medium; }
		if ( $hips >= 48 ) { $hipsMeasure = $large;  }

        $highestValue = Math.max($bust, $waist, $hips);
        $lowestValue = Math.min($bust, $waist, $hips);
        $difference = $highestValue - $lowestValue;
		$src_img = '';
        if ( $difference <= 5 ){
          $shapeType = "I-Shape";
		  $src_img = '/wp-content/themes/aqua/assets/images/body/IWom.jpg';
        }

		
		if($waist - $bust > 5 && $hips - $bust > 5) { 
			$shapeType = "A-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/AWom.jpg';
		} 

		if ($bust - $waist > 5 && $hips - $waist > 5) { 
			$shapeType = "X-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/XWom.jpg';
		}    
		if ($waist - $bust > 5 && $waist - $hips > 5) { 
			$shapeType = "V-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/VWom.jpg';
		}
		if ($hips - $waist > 5 && $hips - $bust > 5) { 
			$shapeType = "A-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/AWom.jpg';
			
		}
        
		if ($bust - $hips > ($hips / 20)) { 
			$shapeType = "V-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/VWom.jpg';

		}
		if (($bust - $hips <= ( $hips / 20)) && ($waist > ($bust * .75))) { 
			$shapeType = "I-shape";
			$src_img = '/wp-content/themes/aqua/assets/images/body/IWom.jpg';
		}
		if ($hips - $bust > ($bust/20)) { 
		   $shapeType = "A-shape";
		   $src_img = '/wp-content/themes/aqua/assets/images/body/AWom.jpg';
		} 
		if (($waist <= ($bust * .75) && $waist <= ($hips * .75))) { 
		   $shapeType = "X-shape";
		   $src_img = '/wp-content/themes/aqua/assets/images/body/XWom.jpg';
		}
	
		
		$body_shape = $shapeType;
    }
	ob_start();
	?>
		<div class="cal_body_result">
			<div class="row">
				<div class="col-sm-6 col-md-7 col-sx-12">
					<div class="cal_content">
					
						<?php if ($body_shape) { ?>
							<p class="body_shape"><?php echo _e('Your body shape: ','aqua'); ?><span><?php echo $body_shape; ?></span></p>	
						<?php } if ($bust) { ?>
							<p class="bust_size"><?php echo _e('Bust Size: ','aqua'); ?><span><?php echo $bust; ?></span></p>
						<?php } if($waist) { ?>
							<p class="waist_size"><?php echo _e('Waist Size: ','aqua'); ?><span><?php echo $waist; ?></span></p>
						<?php } if($hips) { ?>
							<p class="hip_size"><?php echo _e('Hip Size: ','aqua'); ?><span><?php echo $hips; ?></span></p>
						<?php } ?>
					</div>
				</div>
				<div class="col-sm-6 col-md-5 col-sx-12">
					<div class="cal_img"><img src="<?php echo $src_img; ?>" alt=""/></div>
				</div>
			</div>
		</div>
	<?php
	wp_reset_postdata();
    $cal_body_content = ob_get_clean();
    echo tb_filtercontent($cal_body_content); exit;
}
