<?php
$tb_options = $GLOBALS['tb_options'];
$background_images = tb_get_object_id('tb_background_footer_image', true );
$bgfooter_option = $tb_options['tb_bg_footer']['background-image'];

if($background_images){
	$bgfooter_option = $background_images;
}
?>
	<?php
		$tb_display_footer = $tb_options['tb_display_footer'];
		if( $tb_display_footer ){
			$tb_footer_layout = tb_get_object_id('tb_footer', true);
			$tb_footer_full = tb_get_object_id('tb_footer_full');
	 ?>
	<div class="tb_footer tb_footer_v3 tb_footer_<?php echo esc_attr($tb_footer_layout);?> " style="background-image: url('<?php echo $bgfooter_option;?>');">

		<div class="container">
			<!-- Start Footer Top -->
			
			<div class="footer-top">
				<div class="row same-height">
					<!-- Start Footer Sidebar Top 1 -->
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 tb_footer_top_once">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Top Widget 1")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 1 -->
					
					<!-- Start Footer Sidebar Top 2 -->
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 tb_footer_top_two">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Top Widget 2")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 2 -->
					
					<!-- Start Footer Sidebar Top 3 -->
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 tb_footer_top_three">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Top Widget 3")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 3 -->
				</div>
				<div class="row">
					<!-- Start Footer Sidebar Top 4 -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tb_footer_top_four text-center">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Top Widget 4")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 4 -->
				
				</div>
			</div>
			<!-- End Footer Top -->
			
		</div>
		<!-- Start Footer Bottom -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<!-- Start Footer Sidebar Bottom Left -->
					<div class="col-sm-12 col-md-6">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Bottom Widget 1")): endif; ?>
					</div>
					<!-- Start Footer Sidebar Bottom Left -->
					
					<!-- Start Footer Sidebar Bottom Right -->
					<div class="col-sm-12 col-md-6">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer 3 Bottom Widget 2")): endif; ?>
					</div>
					<!-- Start Footer Sidebar Bottom Right -->
				</div>
			</div>
		</div>
		<!-- End Footer Bottom -->
	</div>
	<?php }?>
</div><!-- #wrap -->
<div style="display: none;">
	<div id="tb_send_mail" class="tb-send-mail-wrap">
		<?php if(is_active_sidebar('tbtheme-popup-newsletter-sidebar')){ dynamic_sidebar("tbtheme-popup-newsletter-sidebar"); }?>
	</div>
</div>