<div id="tb-testimonial-image" class="flexslider" >
	<ul class="slides">
	<?php while ( $wp_query->have_posts() ) { $wp_query->the_post();
		$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
		$image_resize = matthewruddy_image_resize( $attachment_image[0], 240, 240, true, false );
		?>

		<li class="ro-item" data-thumb ="<?php echo esc_attr($image_resize['url']); ?>">
			
			<div class="ro-content">
				<?php if($show_excerpt) echo tb_custom_excerpt($excerpt_length , $excerpt_more); ?>
				<?php if($show_title) { ?>
					<div class="ro-name clearfix">
						<p><?php the_title(); ?></p>
					</div>
				<?php } ?>
			</div>
			<?php if($show_image) { ?>
				<div class="ro-image">
					<img src="<?php echo esc_url($image_resize['url']); ?>" alt="img-testimonial">
				</div>
			<?php } ?>
		</li>
	
	<?php } ?>
	</ul>
</div>
