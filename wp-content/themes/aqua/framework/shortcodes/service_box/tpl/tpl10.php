
<div class="ro-service-item-10 clearfix">
	<div class="tb-image">
		<?php
			$attachment_image = wp_get_attachment_image_src($image, 'full');
			$image_resize = matthewruddy_image_resize( $attachment_image[0], 200 , 200 , true, false );
			echo '<img class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
		?>
		<div class="tb-icon">
			<?php echo wp_get_attachment_image($icon, 'full'); ?>
			<?php echo wp_get_attachment_image($icon_active, 'full'); ?>
		</div>
	</div>
	<div class="tb-content">
		<?php echo '<p class="sub_title">'.esc_html($subtitle).'</p>' ?>
		<?php echo '<h4>'.esc_html($title).'</h4>' ?>
		<?php echo '<p>'.$desc.'</p>'; ?>
	</div>
</div>