<?php
	$bg_style = '';
	if($tpl3_bg) $bg_style = 'style="background-color: '.esc_attr($tpl3_bg).';"';
?>
<div class="ro-service-item-9 clearfix">
	<div class="tb-icon" <?php echo ' '.$bg_style; ?>>
		<?php echo wp_get_attachment_image($image, 'full'); ?>
	</div>
	<div class="tb-content">
		<?php echo '<h3>'.esc_html($title).'</h3>' ?>
		<?php if( ! empty( $content ) ) echo '<div class="tb-detail">'.$content.'</div>';?>
	</div>
</div>