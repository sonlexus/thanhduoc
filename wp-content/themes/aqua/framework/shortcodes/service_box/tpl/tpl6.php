<div class="ro-service-item-6 text-center">
	<div class="ro-service-content">
		<?php if(!empty($image)):?>
		<div class="img-inner"><?php
		echo wp_get_attachment_image($image, 'full'); ?>
		</div>
		<?php endif; echo '<h4>'.esc_html($title).'</h4>' ?>
		<?php echo '<p class="subtitle">'.esc_html($subtitle).'</h4>' ?>
		<?php echo '<p>'.$desc.'</p>'; ?>
		<a href="<?php echo esc_url($ex_link); ?>"><?php echo __('View Details','aqua')?></a>
	</div>
</div>