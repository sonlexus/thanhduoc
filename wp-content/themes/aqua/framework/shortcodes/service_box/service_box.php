<?php
function ro_service_box_func($atts, $content = null) {
    extract(shortcode_atts(array(
        'tpl' => 'tpl1',
		'tpl1_style' => 'img_left',
		'tpl1_desc_bg' => '',
		'tpl3_bg' => '',
		'title' => '',
		'subtitle' => '',
		'txt_button' => '',
		'active' => 0,
		'icon'  => '',
		'icon_active'  => '',
        'image' => '',
        'desc' => '',
        'ex_link' => '#',
        'el_class' => ''
    ), $atts));
	$content = wpb_js_remove_wpautop($content, true);
    $class = array();
	$class[] = 'ro-service-wrap';
	$class[] = $tpl;
	$class[] = $el_class;
	if($active){
		$class[] = 'active';
	}
	if( $tpl == 'tpl7' ){
		wp_enqueue_script('waypoints', URI_PATH . '/assets/js/waypoints.min.js',array(),"1");
		wp_enqueue_script('counterup', URI_PATH . '/assets/js/jquery.counterup.min.js',array(),"1");
		
		if(!empty($image)) $class[] = 'has_image';
	}
    ob_start();
    ?>
		<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
			<?php include "tpl/{$tpl}.php"; ?>
		</div>
		
    <?php
    return ob_get_clean();
}
if(function_exists('insert_shortcode')) { insert_shortcode('service_box', 'ro_service_box_func');}
