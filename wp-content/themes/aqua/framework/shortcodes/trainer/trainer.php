<?php
function tb_trainer_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
		'category' => '',
		'post_type' => 'trainer',
        'posts_per_page' => -1,
		'columns' =>  4,
		'crop_image' => 1,
        'width_image' => 373,
        'height_image' => 473,
        'show_social' => 1,
        'show_title' => 1,
        'show_position' => 1,
		'show_pagination' => 0,
		'pos_pagination' => 'text-center',
		'orderby'           	=> 'none',
        'order'             	=> 'none',
        'el_class' => ''
    ), $atts));

    $class[] = 'tb-trainer';
    $class[] = $post_type;
    $class[] = $el_class;
    $class[] = $tpl;
	
    //$category = 109;
	//var_dump($category);
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
		
     if (isset($category) && $category != '') {

        $cats = explode(',', $category);

        $category = array();

        foreach ((array) $cats as $cat) :

        $category[] = trim($cat);

        endforeach;

        $args['tax_query'] = array(

                array(

                    'taxonomy' => 'trainer_category',

                    'field' => 'id',

                    'terms' => $category

                )

        );

    }
    $wp_query = new WP_Query($args);
    
    ob_start();
    if ( $wp_query->have_posts() ) {
    ?>
    <div class="row <?php echo esc_attr(implode(' ', $class)); ?>">
		<?php
			$loop = 0;
			$class_columns = array();
			switch ($columns) {
				case 1:
					$class_columns[] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
					break;
				case 2:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
					break;
				case 3:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
					break;
				case 4:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
				default:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
			}
			
			
			while ( $wp_query->have_posts() ) { $wp_query->the_post();
				echo '<div class=" '.esc_attr(implode(' ', $class_columns)).'">';
						include $tpl.'.php';
				echo '</div>';
			}
		?>
		<div style="clear: both;"></div>
        <?php if($show_pagination){ ?>
			<nav class="pagination number <?php echo esc_attr($pos_pagination); ?>" role="navigation">
				<?php
					$big = 999999999; // need an unlikely integer

					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages,
						'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'aqua' ),
						'next_text' => __( '<i class="fa fa-angle-right"></i>', 'aqua' ),
					) );
				?>
			</nav>
		<?php } ?>
    </div>
    <?php
    }else {
            echo 'Post not found!';
    } 
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) { insert_shortcode('tb_trainer', 'tb_trainer_func'); }
