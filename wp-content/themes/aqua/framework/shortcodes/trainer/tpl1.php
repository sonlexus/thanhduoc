<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if (has_post_thumbnail()) { ?>
		<div class="tb-blog-image">
			<?php 
			$image_full = '';
			if(has_post_thumbnail()){
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
				$image_full = $attachment_image[0];
				if($crop_image){
					$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
					echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
				}else{
					the_post_thumbnail();
				}
			}
			?>
		</div>
	<?php } ?>
	<div class="tb-content-block text-center">
		<?php if( $show_position ){
			$jws_position = esc_attr( get_post_meta(get_the_ID(), 'tb_trainer_position', true) );
			if($jws_position) echo '<p class="tb-position">'. $jws_position.'</p>';
		}
		?>
		<?php if($show_title) echo tb_theme_title_render(); ?>
		
	</div>
</article>