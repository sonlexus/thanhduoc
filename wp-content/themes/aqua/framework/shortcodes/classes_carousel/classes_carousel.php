<?php
function tb_classes_carousel_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'post_type' => 'classes',
        'posts_per_page' => -1,
        'classes_category' => '',
		'columns' =>  4,
        'crop_image' => 0,
        'width_image' => 530,
        'height_image' => 250,
        'show_cate' => 1,
		'show_price' => 1,
        'show_title' => 1,
        'show_info' => 1,
        'orderby' => 'none',
        'order' => 'none',
		'ob_animation' => 'wrap',
		'animation' => '',
        'el_class' => ''
    ), $atts));
    
	$style_wrap = array();
	$class = array();
	
	wp_enqueue_style('owl-carousel', URI_PATH . "/assets/vendors/owl-carousel/owl.carousel.css",array(),"");
	wp_enqueue_script( 'owl.carousel.min', URI_PATH.'/assets/vendors/owl-carousel/owl.carousel.min.js', array('jquery') );
	
	
	$class[] = 'tb-classes-carousel'. intval( $columns );
	$category = $classes_category;
	$taxonomy = 'classes_category';
           
    $cl_effect = getCSSAnimation($animation);
    
    $class[] = 'tb-classes tb-classes-carousel';
 
	if($ob_animation == 'wrap') $class[] = $cl_effect;
    $class[] = $el_class;
    $class[] = $tpl;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
    if (isset($category) && $category != '') {
        $cats = explode(',', $category);
        $category = array();
        foreach ((array) $cats as $cat) :
        $category[] = trim($cat);
        endforeach;
        $args['tax_query'] = array(
                                array(
                                    'taxonomy' => $taxonomy,
                                    'field' => 'id',
                                    'terms' => $category
                                )
                        );
    }
    $wp_query = new WP_Query($args);
    
    ob_start();
	if ( $wp_query->have_posts() ) {
    ?>
    <div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<div class="owl-carousel">
			<?php
				while ( $wp_query->have_posts() ) { $wp_query->the_post();
					include $tpl.'.php';	
				}
			?>
		</div>
    </div>
    <?php
    }else {
		echo 'Post not found!';
    }
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) {
	insert_shortcode('tb_classes_carousel', 'tb_classes_carousel_func');
}
