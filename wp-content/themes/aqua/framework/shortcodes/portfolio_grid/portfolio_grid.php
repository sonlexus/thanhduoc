<?php

function jws_theme_portfolio_func($atts, $content = null) {

    $atts = shortcode_atts(array(
	
		'tpl'  => 'tpl1',

        'category' => '',

		'posts_per_page' => -1,
		
		'show_title' => 1,

		'show_excerpt' => 1,

		'orderby' => 'none',

        'order' => 'none',

        'el_class' => '',


    ), $atts);
	
    extract( $atts );

    $class = array();

    $parent_class[] = 'grid-portfolio';
	
	wp_enqueue_script('masonry', URI_PATH . '/assets/js/masonry.pkgd.min.js',array(),'');
	//wp_enqueue_script('isotope', URI_PATH . '/assets/js/vendors/isotope/isotope.pkgd.min.js',array(),"2.1.5");
    $class[] = $el_class;
	
    $parent_class[] = $tpl;

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = array(

        'posts_per_page' => $posts_per_page,

        'paged' => $paged,

        'orderby' => $orderby,

        'order' => $order,

        'post_type' => 'portfolio',

        'post_status' => 'publish');

    if (isset($category) && $category != '') {

        $cats = explode(',', $category);

        $category = array();

        foreach ((array) $cats as $cat) :

        $category[] = trim($cat);

        endforeach;

        $args['tax_query'] = array(

                array(

                    'taxonomy' => 'portfolio_category',

                    'field' => 'id',

                    'terms' => $category

                )

        );

    }

    foreach( $args as $k=>$v ){
    	if( isset( $atts[$k] ) ) unset( $atts[$k] );
    }

    $p = new WP_Query($args);

    ob_start();

	if ( $p->have_posts() ) {

	?>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
		<div id="tb-list-portfolio" class="<?php echo esc_attr(implode(' ', $class)); ?>">
		
			<div id="portfolio-container" class="tb-grid-content tb-portfolio">
				<?php $i = 0; ?>
				<div class="<?php echo esc_attr(implode(' ',$parent_class));?>">
					<?php include $tpl.".php"; 	?>
				</div>
				<div style="clear: both"></div>
			</div>


		</div>

	<?php

	}

    return ob_get_clean();

}



if(function_exists('insert_shortcode')) { insert_shortcode('tb_portfolio_grid', 'jws_theme_portfolio_func'); }

