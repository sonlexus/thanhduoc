<?php

	$lists_thumb = array('gallery-horizontal-thumb'=>array(3,4),'gallery-vertical-thumb'=>array(2,5),'gallery-normal-thumb'=>array(1,6),'gallery-small-thumb'=>array(0,7));
	
	while( $p->have_posts() ) :  $p->the_post();
	$terms = wp_get_post_terms(get_the_ID(), 'gallery_category');

		if ( !empty( $terms ) && !is_wp_error( $terms ) ){

			$term_list = array();

			foreach ( $terms as $term ) {
				$term_list[] = $term->slug;
			}

		}
		$j = $i % 8;
		$thumb = 'gallery-normal-thumb';
		foreach( $lists_thumb as $k=>$thumbs ){
			if( array_search( $j, $thumbs) !== false ){
				$thumb = $k;
				break;
			}
		}
		$width = tb_get_image_width( $thumb );
		//echo $width;
		$full = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' );
		if( $full ):
		?>
		<div class="grid-item <?php echo esc_attr($thumb);?>">
			<div class="tb-content">
				<?php
				if($show_excerpt): the_excerpt();
				endif;
				if($show_title): ?> 
					<h4> <?php the_title(); ?> </h4>
				<?php endif; ?>
				<a class="cb-popup view-image" title="<?php the_title() ?>" href="<?php echo esc_url( $full[0] );?>">
					<i class="ion-ios-plus-empty"></i>
				</a>
			</div>
			<?php the_post_thumbnail( $thumb ); ?>
		</div>
		<?php
		$i++;
		endif;
	endwhile;
 ?>