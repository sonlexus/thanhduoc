<?php

	$lists_thumb = array('gallery-big-thumb'=>array(0,3,5,6,8,11,13,14),'gallery-thumb'=>array(1,2,4,7,9,10,12,15));
	while( $p->have_posts() ) :  $p->the_post();
	$terms = wp_get_post_terms(get_the_ID(), 'gallery_category');

		if ( !empty( $terms ) && !is_wp_error( $terms ) ){

			$term_list = array();

			foreach ( $terms as $term ) {

				$term_list[] = $term->slug;

			}

		}
		$j = $i % 8;
		$thumb = 'gallery-thumb';
		foreach( $lists_thumb as $k=>$thumbs ){
			if( array_search( $j, $thumbs) !== false ){
				$thumb = $k;
				break;
			}
		}
		$width = tb_get_image_width( $thumb );
		
		$full = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' );
		if( $full ):
		?>
		<div style="width:<?php echo intval( $width/480  * 25);?>%">
			<div class="tb-content">
				<?php
				if($show_excerpt): the_excerpt();
				endif;
				if($show_title):
				?> 
					<h4> <?php the_title(); ?> </h4>
				
				<?php endif; ?>
				
				<a class="cb-popup view-image" title="<?php the_title() ?>" href="<?php echo esc_url( $full[0] );?>">
					<i class="ion-ios-plus-empty"></i>
				</a>
			</div>
			<?php the_post_thumbnail( $thumb ); ?>
		</div>
		<?php
		$i++;
		endif;
	endwhile;
 ?>