<?php

function jws_theme_gallery_func($atts, $content = null) {

    $atts = shortcode_atts(array(
	
		'tpl'  => 'tpl1',

        'category' => '',

		'posts_per_page' => -1,
		
		'show_title' => 1,

		'show_excerpt' => 1,
		
		'show_pagination' => 0,
		
		'pos_pagination' => 'text-center',

		'orderby' => 'none',

        'order' => 'none',

        'el_class' => '',


    ), $atts);
	
    extract( $atts );

    $class = array();

    $parent_class[] = 'grid-gallery';
	
	if($tpl == 'tpl2'){
		$parent_class[] = 'row';
		//wp_enqueue_script('isotope', URI_PATH . '/assets/js/vendors/isotope/isotope.pkgd.min.js',array(),"2.1.5");
		wp_enqueue_script('masonry', URI_PATH . '/assets/js/masonry.pkgd.min.js',array(),'');
	}
	
    $class[] = $el_class;
	
    $parent_class[] = $tpl;

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = array(

        'posts_per_page' => $posts_per_page,

        'paged' => $paged,

        'orderby' => $orderby,

        'order' => $order,

        'post_type' => 'gallery',

        'post_status' => 'publish');

    if (isset($category) && $category != '') {

        $cats = explode(',', $category);

        $category = array();

        foreach ((array) $cats as $cat) :

        $category[] = trim($cat);

        endforeach;

        $args['tax_query'] = array(

                array(

                    'taxonomy' => 'gallery_category',

                    'field' => 'id',

                    'terms' => $category

                )

        );

    }

    foreach( $args as $k=>$v ){
    	if( isset( $atts[$k] ) ) unset( $atts[$k] );
    }

    $p = new WP_Query($args);

    ob_start();

	if ( $p->have_posts() ) {

	?>
		<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
		<div id="tb-list-gallery" class="<?php echo esc_attr(implode(' ', $class)); ?>">
		
			<div id="gallery-container" class="tb-grid-content tb-gallery">
				<?php
					$i = 0;
					?>
					<div class="<?php echo esc_attr(implode(' ',$parent_class));?>">
						<?php include $tpl.".php"; 	?>
					</div>
					<div style="clear: both"></div>
			</div>


		</div>
		<div style="clear: both;"></div>
        <?php if($show_pagination){ ?>
			<nav class="pagination number <?php echo esc_attr($pos_pagination); ?>" role="navigation">
				<?php
					$big = 999999999; // need an unlikely integer

					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $p->max_num_pages,
						'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'aqua' ),
						'next_text' => __( '<i class="fa fa-angle-right"></i>', 'aqua' ),
					) );
				?>
			</nav>
		<?php } 
	}

    return ob_get_clean();

}



if(function_exists('insert_shortcode')) { insert_shortcode('gallery_type', 'jws_theme_gallery_func'); }

