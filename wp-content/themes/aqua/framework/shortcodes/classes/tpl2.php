<?php
global $tb_options;
$image_default = isset($tb_options['tb_blog_image_default']) ? $tb_options['tb_blog_image_default'] : '';

$start_time = get_post_meta(get_the_ID(), 'tb_start_time', true).':00';
$end_time = get_post_meta(get_the_ID(), 'tb_end_time', true).':00';
$tb_trainer = get_post_meta(get_the_ID(), 'tb_trainer', true);
$tb_price = get_post_meta(get_the_ID(), 'tb_price', true);


?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if (has_post_thumbnail() || $image_default) { ?>
        <div class="tb-blog-image">
            <?php
			$image_full = '';
			if(has_post_thumbnail()){
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
				$image_full = $attachment_image[0];
				if($crop_image){
					$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
					echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
				}else{
					the_post_thumbnail();
				}
			}else{
				if($image_default['url']){
					$image_full = $image_default['url'];
					if($tb_blog_crop_image){
						$image_resize = matthewruddy_image_resize( $image_default['url'], $width_image, $height_image, true, false );
						echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
					}else{
						echo '<img alt="Image-Default" class="attachment-thumbnail wp-post-image" src="'. esc_attr($image_default['url']) .'">';
					}
				}
			}
			?>
			<div class="colorbox-wrap">
				<div class="colorbox-inner">
					<?php if($show_popup) : ?><a href="<?php the_permalink(); ?>" class="cb-popup view-detail" title="<?php the_title() ?>">
						<i class="fa fa-link"></i>
					</a>
					<a class="cb-popup view-image" title="<?php the_title() ?>" href="<?php echo esc_url($image_full); ?>">
						<i class="ion-arrow-expand"></i>
					</a>
					<?php endif ?>
				</div>
			</div>
        </div>
    <?php } ?>
	<div class="tb-content-block">
	
		<?php if($show_title) echo tb_theme_title_render(); ?>
		
		<?php if($show_info) { ?>
			<div class="tb-info">
				<div class="tb-time">
					<i class="ion-ios-clock-outline"></i><span><?php echo esc_attr($start_time) .' - '. esc_attr($end_time); ?></span>
				</div>
				<div class="tb-trainner">
					<i class="ion-ios-person-outline"></i><span><?php echo esc_attr($tb_trainer); ?></span>
				</div>
				
			</div>
		<?php } ?>
		<?php if($show_price) { ?>
			<div class="tb-price">
				<?php echo $tb_price; ?>
			</div>
		<?php } ?>
		<div style="clear: both"></div>
	</div>
</article>