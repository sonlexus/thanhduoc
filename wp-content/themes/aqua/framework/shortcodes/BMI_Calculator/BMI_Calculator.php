<?php
function ro_bmi_calculator_func($atts, $content = null) {
    extract(shortcode_atts(array(
		'tpl'  => 'tpl1',
		'formula' => 'metric',
        'el_class' => '',
    ), $atts));
			
    $class = array();
    $class[] = 'ro-bmi-calculator';
    $class[] = $el_class;
    $class[] = $tpl;
	
    ob_start();
    ?>
	<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<form name="form" action="" method="get">
		  <input type="text" name="weight" id="weight" placeholder="<?php echo __("Weight","aqua");?>">
		  <input type="text" name="height" id="height" placeholder="<?php echo __("Height","aqua");?>">
		  <select id="formula">
			  <option value="metric"><?php echo __("Metric BMI","aqua");?></option>
			  <option value="imperial" selected="selected"><?php echo __("Imperial BMI","aqua");?></option>
		  </select>
		  <select id="gender">
			  <option value="male"><?php echo __("Male","aqua");?></option>
			  <option value="female" selected="selected"><?php echo __("Female","aqua");?></option>
		  </select>
		  
		  <input type="submit" name="submit" value="<?php echo __("Calculator Now","aqua");?>" class="btn-calc" >
		  <div class="ro-result">
				<p class="title-result"><?php echo __('Body Mass Index Result','aqua'); ?></p>
				<input type="text" name="result" id="result" placeholder="<?php echo __("Result","aqua");?>" disabled>
				<span class="result-measure"><?php echo _e('Kg/m<sup>2</sup>','aqua'); ?></span>
		  </div>
		</form>
		<script>
			jQuery('document').ready(function() {
				function getResult(){
					var height = jQuery('#height').val();
					var weight = jQuery('#weight').val();
					var formula = jQuery('#formula').val();
					var gender = jQuery('#gender').val();
					var metric_bmi = (weight / ( height * height ));
					var imperial_bmi = (weight / ( height * height )) * 703;
					
					if(formula == 'metric'){
						result = metric_bmi;
					}else result = imperial_bmi;
					return result;
				}
				
				jQuery('.btn-calc').click(function() {
					document.getElementById('result').value = getResult().toFixed(2);
					return false;
				});
			});
		
		</script>
		<div style="clear: both"></div>
	</div>
    <?php
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) { insert_shortcode('bmi_calculator', 'ro_bmi_calculator_func'); }
