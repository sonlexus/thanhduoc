<?php 
    $bust = $waist = $hips = 0;
	if(isset($_GET['bust']) && isset($_GET['waist']) && isset($_GET['hips']) ){
		$bust = $_GET['bust'];
		$waist = $_GET['waist'];
		$hips = $_GET['hips'];
	}
?>
<div class="cal_body_result">
	<div class="row">
		<div class="col-sm-6 col-md-7 col-sx-12">
			<div class="cal_content">
				<p class="body_shape"><?php echo _e('Your Body Shape: ','aqua'); ?><span></span></p>
				<p class="bust_size"><?php echo _e('Bust Size: ','aqua'); ?><span></span></p>
				<p class="waist_size"><?php echo _e('Waist Size: ','aqua'); ?><span></span></p>
				<p class="hip_size"><?php echo _e('Hip Size: ','aqua'); ?><span></span></p>
			</div>
		</div>
		<div class="col-sm-6 col-md-5 col-sx-12">
			<div class="cal_img">
				<img src="/wp-content/themes/aqua/assets/images/body/XWom.jpg" alt="">
			</div>
		</div>
	</div>
</div>
<div class="cal_body_form">
	<div class="row">
		<div class="col-sm-6 col-md-7 col-sx-12">
			<div class="wbp-title">
				<h3><?php echo tb_filtercontent($title);?></h3>
				<p><?php echo tb_filtercontent($subtitle);?></p>
			</div>
			<div style="clear: both"></div>
			<form name="form" action="" method="get">
			  <input type="text" name="bust" id="bust" placeholder="<?php echo __("Bust (cm)","aqua");?>" value="<?php echo $bust; ?>">
			  <input type="text" name="waist" id="waist" placeholder="<?php echo __("Waist (cm)","aqua");?>" value="<?php echo $waist; ?>">
			  <input type="text" name="hips" id="hips" placeholder="<?php echo __("Hips (cm)","aqua");?>" value="<?php echo $hips; ?>">
			  <input type="submit" name="submit" value="<?php echo __("Calculator Now","aqua");?>" class="btn-calc" >
			</form>
		</div>
		<div class="col-sm-6 col-md-5 col-sx-12">
			<div class="form_cal_img">
				<img src="/wp-content/themes/aqua/assets/images/body/WManPH.jpg">
			</div>
		</div>
	</div>
</div>
<script>
	jQuery('document').ready(function() {
		jQuery("#bust").focusin(function() {
			jQuery(".form_cal_img").empty();
			jQuery('.form_cal_img').html('<img src="/wp-content/themes/aqua/assets/images/body/WManBust.jpg">');
		});
		jQuery("#waist").focusin(function() {
			jQuery(".form_cal_img").empty();
			jQuery('.form_cal_img').html('<img src="/wp-content/themes/aqua/assets/images/body/WManWaist.jpg">');
		});
		
		jQuery("#hips").focusin(function() {
			jQuery(".form_cal_img").empty();
			jQuery('.form_cal_img').html('<img src="/wp-content/themes/aqua/assets/images/body/WManHip.jpg">');
		});
	});

</script>
<div style="clear: both"></div>