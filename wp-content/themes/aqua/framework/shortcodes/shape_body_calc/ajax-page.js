!(function($){
	$( window ).load(function() {
		var _cal_form_result = $('.cal_body_result');
		if( _cal_form_result.lenght === 0 ) return;
		var _cal_form = $('.cal_body_form');
		if( _cal_form.lenght === 0 ) return;
		var _btn_cal = _cal_form.find('.btn-calc');
		if( _btn_cal.lenght === 0 ) return;
		_btn_cal.on('click', function(e){
			e.preventDefault();
			
			var _form = $(this).closest('form');
			if( _form.length === 0 ) return;
			
			var data = {},
				_this = $(this);
				
			data.bust = _form.find('#bust').val();
			data.waist = _form.find('#waist').val();
			data.hips = _form.find('#hips').val();
			_cal_form_result.addClass('blog-more-ajax-loading');
			$.ajax({
				type: "POST",
				url: variable_js.ajax_url,
				data: {action: 'tb_render_cal_body', data: data},
				success: function(data){
					_cal_form_result.removeClass('blog-more-ajax-loading');
					_cal_form_result.empty().append(data);
				}
			})
		})
	})
})(jQuery) 