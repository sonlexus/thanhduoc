<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="ro-relate-posts-item">
		<?php if($show_image) { ?>
			<a href="<?php the_permalink(); ?>">
				<div class="ro-image"><?php the_post_thumbnail('full'); ?></div>
			</a>
		<?php } ?>
		<div class="ro-content">
			<?php if($show_title) { ?>
				<h4><a class="ro-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<?php } ?>
			<?php if($show_info) { ?>
				<p class="ro-info"><?php _e('By: ', 'aqua'); the_author(); ?>  /  <?php echo get_the_date('l, F, j, Y'); ?></p>
			<?php } ?>
			<?php if($show_excerpt) the_excerpt(); ?>
		</div>
	</div>
</article>