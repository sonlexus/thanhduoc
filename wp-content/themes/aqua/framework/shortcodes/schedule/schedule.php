<?php
function tb_schedule_func($atts) {
    extract(shortcode_atts(array(
		'begin' => 7,
		'end' => 20,
		'datestart' => '2017-01-13',
		'dateend' => '2017-01-25',
        'el_class' => ''
    ), $atts));

    $class = array();
    $class[] = 'tb-schedule';      
    $class[] = $el_class;
	wp_enqueue_script('schedule.page.ajax', URI_PATH_FR . '/shortcodes/schedule/ajax-page.js');
	wp_localize_script( 'schedule.page.ajax', 'variable_js', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
	$dayofweek = array(
		'' => '&nbsp;',
		'monday' => esc_html__('Monday', 'aqua'),
		'tuesday' => esc_html__('Tuesday', 'aqua'),
		'wednesday' => esc_html__('Wednesday', 'aqua'),
		'thurday' => esc_html__('Thurday', 'aqua'),
		'friday' => esc_html__('Friday', 'aqua'),
		'saturday' => esc_html__('Saturday', 'aqua'),
		'sunday' => esc_html__('Sunday', 'aqua'),
	);
	$time_array = array();
	for($begin; $begin<= $end; $begin++){
		$time_array[$begin] = $begin .':00';
	}
	
	$query_args = array(
		'post_type' => 'classes',
		'post_status' => 'publish'
	);
	 ob_start();
	?>
        <div class="schedule-container">
            <form class="schedule-filter" action="" method="post">
                <input type="hidden" name="timestamp" value="">
                <div>
                    <ul class="month-nav">
                        
						<input type="date" id="tb_date_start" class="bt-date-picker" name="tb_date_start" value="<?php echo esc_attr($datestart);?>">
						<span><?php echo _e('- to - ','aqua');?></span>
						<input type="date" id="tb_date_end" class="bt-date-picker" name="tb_date_end" value="<?php echo esc_attr($dateend);?>">
						<a class="view_schedule" href="#"><?php echo _e('View Schedulle','aqua');?></a>
                    </ul>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-schedule">
                    <thead>
                        <tr>
							<?php foreach ($dayofweek as $key => $value) { ?>
                                <th><?php echo esc_attr($value); ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach ($time_array as $key => $value) : ?>
                            <tr>
							
                                <?php
									foreach ($dayofweek as $day_key => $day) {	?>
                                    <?php
									
                                    if ($day_key == '') : echo "<td style='background-color: #fff;'><span class='tb-htime'>".esc_attr($value)."</span></td>";
                                    else :
										$meta_query = array();
                                        $meta_query[] = array(
                                            'key' => 'tb_start_time',
                                            'value' => $key,
                                            'compare' => '=',
                                        );
										
                                        $meta_query[] = array(
                                            'key' => 'tb_date',
                                            'value' => array($datestart, $dateend),
                                            'type' => 'DATE',
                                            'compare' => 'BETWEEN',
                                        );
                                        $query_args['meta_query'] = $meta_query;
                                        $class_posts = new WP_Query($query_args);
										
                                        if ($class_posts->have_posts()) {
                                            ?>
                                            <?php
                                            while ($class_posts->have_posts()) : $class_posts->the_post();
                                               $start_time = get_post_meta(get_the_ID(), 'tb_start_time', true) .':00';
                                               $end_time = get_post_meta(get_the_ID(), 'tb_end_time', true) .':00';
                                               $tb_day_of_week = get_post_meta(get_the_ID(), 'tb_day_of_week', true);
                                               $tb_date = get_post_meta(get_the_ID(), 'tb_date', true);
											   
											   if( in_array( $day_key, $tb_day_of_week ) ){

                                                ?> 
                                                <td <?php post_class(); ?>>
                                                    <div class="has_class">
														<span class="tb_time">
															<?php echo $start_time .' - '. $end_time; ?>
														</span>
														<a href="<?php the_permalink(); ?>">
															<?php echo the_title();?>
														</a>
                                                    </div>
                                                </td>
											   <?php } else { ?>
													<td style="background-color: #fff;">
														<span class="tb_empty">
															<?php echo __('Break','aqua');?>
														</span>
													 </td>
												<?php } 
												break;
                                            endwhile; ?>
											 <a class="hidden table-args" data-args='<?php echo json_encode( $query_args );?>' data-atts='<?php echo json_encode( $atts );?>' ></a> 
                                    <?php } else { ?>
                                            <td style="background-color: #fff;">
												<span class="tb_empty">
													<?php echo __('Break','aqua');?>
												</span>
											</td>
                    <?php
                    }
                    wp_reset_postdata();
                    ?>
                <?php endif; ?>
            <?php } ?>
                            </tr>
        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

if(function_exists('insert_shortcode')) { insert_shortcode('schedule', 'tb_schedule_func'); }


add_action( 'wp_ajax_render_schedule_table', 'render_schedule_table' );
add_action( 'wp_ajax_nopriv_render_schedule_table', 'render_schedule_table' );

function render_schedule_table(){
	global $wpdb, $pagenow;
	$data = (array)$_POST['params'];
	
	//if( empty( $data['args']) || empty( $data['atts']) ) return;
	
	$query_args = (array)$data['args'];
	
	$atts =  wp_parse_args( (array)$data['atts'], array(
		'begin' => 7,
		'end' => 20,
		'datestart' => '2017-01-13',
		'dateend' => '2017-01-25',
        'el_class' => ''

     ) );
	extract( $atts );
	
    if( isset( $data['datestart'] ) && $data['dateend'] ){
    	$datestart = $data['datestart'];
    	$dateend = $data['dateend'];
    }
	
	$class = array();
	
	$dayofweek = array(
		'' => '&nbsp;',
		'monday' => esc_html__('Monday', 'aqua'),
		'tuesday' => esc_html__('Tuesday', 'aqua'),
		'wednesday' => esc_html__('Wednesday', 'aqua'),
		'thurday' => esc_html__('Thurday', 'aqua'),
		'friday' => esc_html__('Friday', 'aqua'),
		'saturday' => esc_html__('Saturday', 'aqua'),
		'sunday' => esc_html__('Sunday', 'aqua'),
	);
	$time_array = array();
	for($begin; $begin<= $end; $begin++){
		$time_array[$begin] = $begin .':00';
	}
	
	$query_args = array(
		'post_type' => 'classes',
		'post_status' => 'publish'
	);

	ob_start();
	?>
                <table class="table table-schedule">
                    <thead>
                        <tr>
							<?php foreach ($dayofweek as $key => $value) { ?>
                                <th><?php echo esc_attr($value); ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach ($time_array as $key => $value) : ?>
                            <tr>
							
                                <?php
									foreach ($dayofweek as $day_key => $day) {	?>
                                    <?php
									
                                    if ($day_key == '') : echo "<td style='background-color: #fff;'><span class='tb-htime'>".esc_attr($value)."</span></td>";
                                    else :
										$meta_query = array();
                                        $meta_query[] = array(
                                            'key' => 'tb_start_time',
                                            'value' => $key,
                                            'compare' => '=',
                                        );
										
                                        $meta_query[] = array(
                                            'key' => 'tb_date',
                                            'value' => array($datestart, $dateend),
                                            'type' => 'DATE',
                                            'compare' => 'BETWEEN',
                                        );
                                        $query_args['meta_query'] = $meta_query;
                                        $class_posts = new WP_Query($query_args);
										
                                        if ($class_posts->have_posts()) {
                                            ?>
                                            <?php
                                            while ($class_posts->have_posts()) : $class_posts->the_post();
                                               $start_time = get_post_meta(get_the_ID(), 'tb_start_time', true) .':00';
                                               $end_time = get_post_meta(get_the_ID(), 'tb_end_time', true) .':00';
                                               $tb_day_of_week = get_post_meta(get_the_ID(), 'tb_day_of_week', true);
                                               $tb_date = get_post_meta(get_the_ID(), 'tb_date', true);
											   
											   if( in_array( $day_key, $tb_day_of_week ) ){

                                                ?> 
                                                <td <?php post_class(); ?>>
                                                    <div class="has_class">
														<span class="tb_time">
															<?php echo $start_time .' - '. $end_time; ?>
														</span>
														<a href="<?php the_permalink(); ?>">
															<?php echo the_title();?>
														</a>
														
                                                    </div>
                                                </td>
											   <?php } else { ?>
													<td style="background-color: #fff;">
														<span class="tb_empty">
															<?php echo __('Break','aqua');?>
														</span>
													 </td>
												<?php } 
												break;
                                            endwhile; ?>
											 <a class="hidden table-args" data-args='<?php echo json_encode( $query_args );?>' data-atts='<?php echo json_encode( $atts );?>' ></a> 
                                    <?php } else { ?>
                                            <td style="background-color: #fff;">
												<span class="tb_empty">
													<?php echo __('Break','aqua');?>
												</span>
											</td>
                    <?php
                    }
                    wp_reset_postdata();
                    ?>
                <?php endif; ?>
            <?php } ?>
                            </tr>
        <?php endforeach; ?>
                    </tbody>
                </table>
	<?php
	wp_reset_postdata();
    $schedule_col = ob_get_clean();
    echo tb_filtercontent($schedule_col); exit;
}
