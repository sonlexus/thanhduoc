<?php
function tb_spa_grid_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'post_type' => 'spa',
        'posts_per_page' => -1,
        'spa_category' => '',
		'columns' =>  3,
        'width_image' => 370,
        'height_image' => 270,
        'crop_image' => 0,
        'show_title' => 1,
        'show_price' => 1,
        'show_desc' => 1,
        'excerpt_length' => 25,
        'excerpt_more' => '',
        'orderby' => 'none',
        'order' => 'none',
		'ob_animation' => 'wrap',
		'animation' => '',
        'el_class' => ''
    ), $atts));
    
	$style_wrap = array();
	$class = array();
	
	$category = $spa_category;
	$taxonomy = 'spa_category';
           
    $cl_effect = getCSSAnimation($animation);
    
    $class[] = 'tb-spa';
    $class[] = $tpl;
 
	if($ob_animation == 'wrap') $class[] = $cl_effect;
    $class[] = $el_class;
    
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
    if (isset($category) && $category != '') {
        $cats = explode(',', $category);
        $category = array();
        foreach ((array) $cats as $cat) :
        $category[] = trim($cat);
        endforeach;
        $args['tax_query'] = array(
                                array(
                                    'taxonomy' => $taxonomy,
                                    'field' => 'id',
                                    'terms' => $category
                                )
                        );
    }
    $wp_query = new WP_Query($args);
    
    ob_start();
    if ( $wp_query->have_posts() ) {
    ?>
    <div class="row <?php echo esc_attr(implode(' ', $class)); ?>" style="<?php echo esc_attr(implode(' ', $style_wrap)); ?>">
		<?php
			$class_columns = array();
			
			switch ($columns) {
				case 1:
					$class_columns[] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
					break;
				case 2:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
					break;
				case 3:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
					break;
				case 4:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
				default:
					$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
					break;
			}
			
			if($ob_animation == 'item') $class_columns[] = $cl_effect;
			$i = 1;
			$count = $wp_query->post_count;
			while ( $wp_query->have_posts() ) { $wp_query->the_post();?>
				
				<div class="<?php echo esc_attr(implode(' ', $class_columns)); ?>">
					<?php include $tpl .".php";	?>
				</div>
				
				<?php
				
				if( $i % $columns == 0 && $i != $count){
					echo '<div style="clear: both"></div>';
					echo '<div class="line"></div>';
				}
				$i++;
			}
		?>
		
    </div>
    <?php
    }else {
            echo 'Post not found!';
    } 
    ?>
    
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) {
	insert_shortcode('tb_spa_grid', 'tb_spa_grid_func');
}
