<?php
function tb_pricing_func($atts, $content = null) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'title' => '',
		'price' =>'',
		'color' =>'',
		'bg_image' => '',
		'sub_title' => '',
        'align' => '',
		'btn_text'=> 'get started',
		'ex_link' => '#',
        'el_class' => '',
    ), $atts));
	$content = wpb_js_remove_wpautop($content, true);
    $class[] = $el_class;
    $class[] = $tpl;

	ob_start();
	?>
		<?php include $tpl.'.php'; ?>
	<?php
	$layout = ob_get_clean();
	return $layout;
}

if(function_exists('insert_shortcode')) { insert_shortcode('pricing', 'tb_pricing_func'); }
