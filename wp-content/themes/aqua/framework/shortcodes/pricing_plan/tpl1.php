<div class="tb-pricing <?php echo esc_attr(implode(' ', $class)); ?>">
	<div class="tb-pricing-content text-center">
	<div class="tb-pricing-top">
		<?php if(!empty($title)): ?><h4><?php echo tb_filtercontent($title);?></h4>
		<?php endif; if(!empty($sub_title)):?>
		<p class="sub_title"><?php echo tb_filtercontent($sub_title);?></p>
		<?php echo tb_filtercontent($price);?>
	</div>
		<?php endif; if( ! empty( $content ) ) echo '<div class="pricing-content">'.$content.'</div>';
		if(!empty($btn_text)): 	?>
		<a class="btn-button" href="<?php echo esc_url($ex_link); ?>"><?php echo esc_html($btn_text);?></a>
		<?php endif; ?>
		<div style="clear: both"></div>
	</div>
</div>