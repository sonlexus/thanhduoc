<?php
function tb_blog_carousel_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'post_type' => 'post',
        'posts_per_page' => -1,
        'category' => '',
        'crop_image' => 1,
		'columns' => 1,
        'width_image' => 300,
        'height_image' => 200,
        'show_title' => 1,
        'show_info' => 1,
        'show_desc' => 1,
        'excerpt_length' => -1,
        'excerpt_more' => '',
        'orderby' => 'none',
        'order' => 'none',
		'ob_animation' => 'wrap',
		'animation' => '',
        'el_class' => ''
    ), $atts));
    
	$class = array();
	$category = $category;
	$taxonomy = 'category';
	
    $cl_effect = getCSSAnimation($animation);

	wp_enqueue_style('owl-carousel', URI_PATH . "/assets/vendors/owl-carousel/owl.carousel.css",array(),"");
	wp_enqueue_script( 'owl.carousel.min', URI_PATH.'/assets/vendors/owl-carousel/owl.carousel.min.js', array('jquery') );
	
    $class[] = 'tb-blog-carousel'. intval( $columns );
    $class[] = 'tb-blog';
    $class[] = $tpl;
    $class[] = $post_type;

	if($ob_animation == 'wrap') $class[] = $cl_effect;
    $class[] = $el_class;
    
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
    if (isset($category) && $category != '') {
        $cats = explode(',', $category);
        $category = array();
        foreach ((array) $cats as $cat) :
        $category[] = trim($cat);
        endforeach;
        $args['tax_query'] = array(
                                array(
                                    'taxonomy' => $taxonomy,
                                    'field' => 'id',
                                    'terms' => $category
                                )
                        );
    }
    $wp_query = new WP_Query($args);
    
    ob_start();
    if ( $wp_query->have_posts() ) {
    ?>
    <div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<?php
			$class_columns = array();
			if($ob_animation == 'item') $class_columns[] = $cl_effect;
			if($tpl == 'tpl3'){?>
				<div class="owl-carousel">
			<?php } else {?>
				<div id="tb-blog-image" class="flexslider" >
				<ul class="slides">
			<?php }
					while ( $wp_query->have_posts() ) { $wp_query->the_post();
						include $tpl.'.php'; 
					}
			if($tpl == 'tpl3'){?>
				</div>
			<?php } else {?>
				</ul>
				</div>
			<?php } ?>
		<div style="clear: both;"></div>
    </div>
    <?php
    }else {
            echo 'Post not found!';
    } 
    ?>
    
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) { insert_shortcode('tb_blog_carousel', 'tb_blog_carousel_func'); }
