<?php
global $tb_options;
$image_default = isset($tb_options['tb_blog_image_default']) ? $tb_options['tb_blog_image_default'] : '';

$event_time = get_post_meta(get_the_ID(), 'tb_time', true);
$event_address = get_post_meta(get_the_ID(), 'tb_address', true);

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if (has_post_thumbnail() || $image_default) { ?>
        <div class="tb-blog-image">
            <?php
			$image_full = '';
			if(has_post_thumbnail()){
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
				$image_full = $attachment_image[0];
				if($crop_image){
					$image_resize = matthewruddy_image_resize( $attachment_image[0], $width_image, $height_image, true, false );
					echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
				}else{
					the_post_thumbnail();
				}
			}else{
				if($image_default['url']){
					$image_full = $image_default['url'];
					if($tb_blog_crop_image){
						$image_resize = matthewruddy_image_resize( $image_default['url'], $width_image, $height_image, true, false );
						echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
					}else{
						echo '<img alt="Image-Default" class="attachment-thumbnail wp-post-image" src="'. esc_attr($image_default['url']) .'">';
					}
				}
			}
			?>
        </div>
    <?php } ?>
	<div class="tb-content-block">
		<?php if($show_time) { ?>
			<div class="tb-time"><?php echo esc_attr($event_time); ?></div>
		<?php } ?>
		<?php if($show_title) echo tb_theme_title_render(); ?>
		
		<?php if($show_address) { ?>
			<div class="tb-address"><i class="ion-ios-location-outline"></i><?php echo esc_attr($event_address); ?></div>
		<?php } ?>
		<div style="clear: both"></div>
	</div>
</article>