<?php
function tb_events_carousel_func($atts) {
    extract(shortcode_atts(array(
		'tpl' => 'tpl1',
        'post_type' => 'events',
        'posts_per_page' => -1,
        'events_category' => '',
		'columns' =>  4,
        'crop_image' => 0,
        'width_image' => 470,
        'height_image' => 346,
        'show_time' => 1,
        'show_title' => 1,
        'show_address' => 1,
        'orderby' => 'none',
        'order' => 'none',
		'ob_animation' => 'wrap',
		'animation' => '',
        'el_class' => ''
    ), $atts));
    
	$style_wrap = array();
	$class = array();
	
	$class[] = 'tb-events-carousel'. intval( $columns );
	$category = $events_category;
	$taxonomy = 'events_category';
           
    $cl_effect = getCSSAnimation($animation);
    
    $class[] = 'tb-events tb-events-carousel';
 
	if($ob_animation == 'wrap') $class[] = $cl_effect;
    $class[] = $el_class;
    $class[] = $tpl;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array(
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'orderby' => $orderby,
        'order' => $order,
        'post_type' => $post_type,
        'post_status' => 'publish');
    if (isset($category) && $category != '') {
        $cats = explode(',', $category);
        $category = array();
        foreach ((array) $cats as $cat) :
        $category[] = trim($cat);
        endforeach;
        $args['tax_query'] = array(
                                array(
                                    'taxonomy' => $taxonomy,
                                    'field' => 'id',
                                    'terms' => $category
                                )
                        );
    }
    $wp_query = new WP_Query($args);
    
    ob_start();
	if ( $wp_query->have_posts() ) {
    ?>
    <div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<div class="owl-carousel">
			<?php
				while ( $wp_query->have_posts() ) { $wp_query->the_post();
					include $tpl.'.php';	
				}
			?>
		</div>
    </div>
    <?php
    }else {
		echo 'Post not found!';
    }
    wp_reset_postdata();
    return ob_get_clean();
}

if(function_exists('insert_shortcode')) {
	insert_shortcode('tb_events_carousel', 'tb_events_carousel_func');
}
