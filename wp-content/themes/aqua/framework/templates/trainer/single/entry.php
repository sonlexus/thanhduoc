<?php
global $tb_options;
$image_default = isset($tb_options['tb_blog_image_default']) ? $tb_options['tb_blog_image_default'] : '';

$tb_address = get_post_meta(get_the_ID(), 'tb_address', true);
$tb_position = get_post_meta(get_the_ID(), 'tb_trainer_position', true);
$tb_phone = get_post_meta(get_the_ID(), 'tb_phone', true);
$tb_email = get_post_meta(get_the_ID(), 'tb_email', true);
$tb_socials = wp_kses_post( get_post_meta( get_the_ID(), 'tb_trainer_social', true ) );

if(is_home()){
	$tb_show_post_title = 1;
	$tb_show_post_desc = 1;
	$tb_show_post_info = 1;
}elseif (is_single()) {
	$tb_trainer_crop_image = isset($tb_options['tb_trainer_crop_image']) ? $tb_options['tb_trainer_crop_image'] : 0;
	$tb_trainer_image_width = (int) isset($tb_options['tb_trainer_image_width']) ? $tb_options['tb_trainer_image_width'] : 470;
	$tb_trainer_image_height = (int) isset($tb_options['tb_trainer_image_height']) ? $tb_options['tb_trainer_image_height'] : 500;
	$tb_trainer_show_post_title = (int) isset($tb_options['tb_classes_show_post_title']) ? $tb_options['tb_classes_show_post_title'] : 1;
	$tb_trainer_show_post_info = (int) isset($tb_options['tb_trainer_post_show_post_info']) ? $tb_options['tb_trainer_post_show_post_title'] : 1;
	$tb_trainer_show_social_share = (int) isset($tb_options['tb_trainer_show_social_share']) ? $tb_options['tb_trainer_show_social_share'] : 1;

	$tb_trainer_link_schdule = isset($tb_options['tb_trainer_link_schdule']) ? $tb_options['tb_trainer_link_schdule'] : "";
	$tb_show_post_desc = 1;
	
}else{
	$tb_trainer_crop_image = isset($tb_options['tb_trainer_crop_image']) ? $tb_options['tb_trainer_crop_image'] : 0;
	$tb_trainer_image_width = (int) isset($tb_options['tb_trainer_image_width']) ? $tb_options['tb_trainer_image_width'] : 470;
	$tb_trainer_image_height = (int) isset($tb_options['tb_trainer_image_height']) ? $tb_options['tb_trainer_image_height'] : 500;
	$tb_show_post_title = (int) isset($tb_options['tb_trainer_show_post_title']) ? $tb_options['tb_trainer_show_post_title'] : 1;
	$tb_show_post_info = (int) isset($tb_options['tb_trainer_show_post_info']) ? $tb_options['tb_trainer_show_post_title'] : 1;
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row"><div class="col-md-5">
    <?php if (has_post_thumbnail() || $image_default['url']) { ?>
        <div class="tb-blog-image">
            <?php
			if(!is_home()){
				$image_full = '';
				if(has_post_thumbnail()){
					$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
					$image_full = $attachment_image[0];
					if($tb_trainer_crop_image){
						$image_resize = matthewruddy_image_resize( $attachment_image[0], $tb_trainer_image_width, $tb_trainer_image_height, true, false );
						echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
					}else{
						the_post_thumbnail();
					}
				}else{
					if($image_default['url']){
						$image_full = $image_default['url'];
						if($tb_trainer_crop_image){
							$image_resize = matthewruddy_image_resize( $image_default['url'], $tb_trainer_image_width, $tb_trainer_image_height, true, false );
							echo '<img style="width:100%;" class="bt-image-cropped" src="'. esc_attr($image_resize['url']) .'" alt="">';
						}else{
							echo '<img alt="Image-Default" class="attachment-thumbnail wp-post-image" src="'. esc_attr($image_default['url']) .'">';
						}
					}
				}
			}
			?>
        </div>
    <?php } ?>
	</div>
	<div class="col-md-7">
	<div class="tb-content-block">
		<div class="tb-heading">
			<div class="tb-title"><?php if($tb_trainer_show_post_title) echo tb_theme_title_render(); ?>

			<?php if($tb_position) echo '<span>'. $tb_position .'</span>'; ?></div>
			<?php if($tb_trainer_link_schdule){ ?>
				<a class="btn-right btn-button" href="<?php echo esc_url($tb_trainer_link_schdule)?>"><?php echo _e('View My Schdule','aqua');?> </a>
			<?php } ?>
		</div>
		<?php if($tb_trainer_show_post_info){?>
			<div class="tb-info">
				<div class="tb-phone">
					<p class="tb-title"><i class="ion-ios-telephone-outline"></i><?php echo _e('Phone: ','aqua');?></p>
					<span><?php echo esc_attr($tb_phone); ?></span>
				</div>
				<div class="tb-email">
					<p class="tb-title"><i class="ion-ios-email-outline"></i><?php echo _e('Email Id: ','aqua');?></p><span><?php echo esc_attr($tb_email); ?></span>
				</div>
				<div class="tb-address">
					<p class="tb-title"><i class="ion-ios-location-outline"></i><?php echo _e('Address: ','aqua');?></p><span><?php echo esc_attr($tb_address); ?></span>
				</div>
				<div class="tb-excerpt">
					<p class="tb-title"><i class="ion-ios-personadd-outline"></i><?php echo _e('Biography: ','aqua');?></p><span><?php echo tb_theme_content_render() ?></span>
				</div>
				<?php if($tb_trainer_show_social_share) {?>
				<div class="tb-socials">
					<p class="tb-title"><i class="ion-ios-color-filter-outline"></i><?php echo _e('Follow me: ','aqua');?></p><div class="socials-list"><?php echo $tb_socials; ?></div>
				</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	</div>
	</div>
</article>