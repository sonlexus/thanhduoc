<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<?php global $tb_options; ?>
<?php if($tb_options['tb_responsive']){ ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<?php } ?>	
<?php wp_head(); ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-78533155-1', 'auto');
ga('send', 'pageview');
</script>
</head>
<?php
$body_class = 'tb_body';
$layout = $tb_options["tb_layout"];
$layout_class = $layout=='boxed'? $body_class .= ' boxed':$body_class .= ' wide';
?>
<body <?php body_class($body_class) ?>>	
<div id="tb_wrapper">
	<?php
		$tb_display_top_sidebar = get_post_meta(get_the_ID(), 'tb_display_top_sidebar', true)?get_post_meta(get_the_ID(), 'tb_display_top_sidebar', true):0;
		if(is_active_sidebar( 'tbtheme-top-sidebar' ) && $tb_display_top_sidebar){
		?>
		<div class="tb_top_sidebar_wrap">
			<?php dynamic_sidebar("Top Sidebar"); ?>
		</div>
		<?php
		}
	?>
	<?php tb_Header(); ?>
	<?php if(is_active_sidebar( 'tbtheme-right-fixed-sidebar' )){ ?>
		<div class="tb_right_fx_wrap">
			<?php dynamic_sidebar("Right Fixed Sidebar"); ?>
		</div>
	<?php } ?>
		