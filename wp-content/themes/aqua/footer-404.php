	<?php global $tb_options; ?>
	<?php if($tb_options['tb_display_footer']){ ?>
	<div class="tb_footer white">
		<div class="container">
			<!-- Start Footer Top -->

			<div class="footer-top">
				<div class="row same-height">
					<!-- Start Footer Sidebar Top 1 -->
					<div class="col-xs-12 col-sm-7 col-md-3 col-lg-3 tb_footer_top_once">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top Widget 1")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 1 -->
					<!-- Start Footer Sidebar Top 2 -->
					<div class="col-xs-12 col-sm-5 col-md-2 col-lg-2 tb_footer_top_two">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top Widget 2")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 2 -->
					<!-- Start Footer Sidebar Top 3 -->
					<div class="col-xs-12 col-sm-5 col-md-2 col-lg-2 tb_footer_top_three">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top Widget 3")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 3 -->
					<!-- Start Footer Sidebar Top 4 -->
					<div class="col-xs-12 col-sm-7 col-md-5 col-lg-5 tb-col4 tb_footer_top_four">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top Widget 4")): endif; ?>
					</div>
					<!-- End Footer Sidebar Top 4 -->
				</div>
			</div>
			<!-- End Footer Top -->
			<!-- Start Footer Bottom -->
			<div class="footer-bottom">
				<div class="row">
					<!-- Start Footer Sidebar Bottom Left -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Bottom Widget 1")): endif; ?>
					</div>
					<!-- Start Footer Sidebar Bottom Left -->
				</div>
			</div>
			<!-- End Footer Bottom -->
		</div>
	</div>
	<?php } ?>
</div><!-- #wrap -->
<a id="tb_back_to_top">
	<span class="go_up">
	<i class="icon-up"></i>
	</span>
</a>
<?php wp_footer(); ?>
<div style="display:none">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1008974072;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1008974072/?guid=ON&amp;script=0"/>
</div>
</noscript>
</div>
</body>